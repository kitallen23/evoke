onmessage = function(event) {
    console.log("Something?");
    importScripts('./highlight.pack.js');
    let result = self.hljs.highlightAuto(event.data);
    postMessage(result.value);
};