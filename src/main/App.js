import React, { Component } from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';
import Firebase from '../utils/Firebase';
import { NoteAPI, CategoryAPI } from "../utils/api";
import PropTypes from "prop-types";
import '../main.css';

import NavBar from '../components/NavBar';
import Menu from '../components/Menu';
import Spinner from '../components/utility/Spinner/Spinner';

import HomeScreen from '../views/HomeScreen';
import CategoryScreen from '../views/CategoryScreen';
import WelcomeScreen from '../views/WelcomeScreen/WelcomeScreen';      // TODO: Edit for Redux?

class App extends Component {

    componentWillMount() {
        Firebase.init();
    }

    componentWillReceiveProps( nextProps ) {
        // Only triggers when user is first loaded
        if ( nextProps.user && this.props.user === null ) {
            this.loadResources().then( ( resp ) => {
                let notesResponse = resp[ 0 ],
                    categoriesResponse = resp[ 1 ];

                // Ensure a response was obtained
                if ( typeof notesResponse === "undefined" ||
                    typeof categoriesResponse === "undefined" ) {
                    this.props.setErrorMsg( "Failed to contact the server." );
                    // TODO: Redirect to "failed to load" page
                    this.props.setAllLoading( false );  // Temporary fix
                    return;
                }

                // NOTES
                if ( notesResponse.status !== "SUCCESS" ) {
                    this.props.setErrorMsg( notesResponse.message );
                    // TODO: Redirect to "failed to load" page
                    this.props.setAllLoading( false );  // Temporary fix
                    return;
                }
                console.log( "DATABASE NOTES:" );
                console.log( notesResponse.notes );
                this.props.addAllNotes( notesResponse.notes );

                // CATEGORIES
                if ( categoriesResponse.status !== "SUCCESS" ) {
                    this.props.setErrorMsg( categoriesResponse.message );
                    // TODO: Redirect to "failed to load" page
                    this.props.setAllLoading( false );  // Temporary fix
                    return;
                }
                this.props.addAllCategories( categoriesResponse.categories );

                // TODO: Count notes related to each category and set in redux

                this.props.initialiseFilters( categoriesResponse.categories );

                // Finally, set all data as loaded
                this.props.setAllLoading( false );

            } ).catch( ( err ) => {
                console.log( err )
            } );
        }
    }

    loadResources = async () => {
        return await Promise.all( [ this.getNotes(), this.getCategories() ] );
    };

    getNotes = async () => {
        // Get notes from API
        return await NoteAPI.GetAllNotes();
    };

    getCategories = async () => {
        // Get categories from API
        return await CategoryAPI.GetAllCategories();
    };

    render() {

        return (
            <div>
                {this.props.loadingUser ? (

                    <div className="loading-wrapper">
                        <NavBar isActive={false} />
                        <Menu />
                        <Spinner scale={1.5} />
                    </div>

                ) : (

                    <div>
                        <NavBar isActive={true} />
                        {this.props.user === null ? (
                            <div className="content-wrapper">
                                <Menu />
                                <Switch>
                                    <Route exact path="/welcome" component={WelcomeScreen} />
                                    <Redirect to="/welcome" />
                                </Switch>
                            </div>
                        ) : (
                            <div>
                                {this.props.loadingAll ? (

                                    <div className="loading-wrapper">
                                        <NavBar isActive={false} />
                                        <Menu />
                                        <Spinner scale={1.5} />
                                    </div>

                                ) : (
                                    <div className="content-wrapper">
                                        <Menu />
                                        <Switch>
                                            <Route exact path="/" component={HomeScreen} />
                                            <Route path="/categories" component={CategoryScreen} />
                                            <Redirect to="/" />
                                        </Switch>
                                    </div>
                                )}
                            </div>
                        )}
                    </div>

                )}
            </div>
        );
    }
}

App.propTypes = {

    user: PropTypes.object,
    loadingAll: PropTypes.bool,
    loadingUser: PropTypes.bool,
    showMenu: PropTypes.bool,
    router: PropTypes.object,

    setUser: PropTypes.func,
    clearUser: PropTypes.func,

    addAllNotes: PropTypes.func,
    addAllCategories: PropTypes.func,

    setAllLoading: PropTypes.func,
    setErrorMsg: PropTypes.func,
};

export default App;