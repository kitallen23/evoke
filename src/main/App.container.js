import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setUser, clearUser } from '../redux/auth/auth.actions';
import { addAllNotes } from '../redux/notes/notes.actions';
import { addAllCategories } from '../redux/categories/categories.actions';
import { setAllLoading, setErrorMsg } from '../redux/utility/utility.actions';
import { initialiseFilters } from '../redux/filter/filter.actions';
import { withRouter } from 'react-router';

import App from './App';

const mapStateToProps = state => ({
    user: state.auth.user,
    loadingAll: state.utility.loading.all,
    loadingUser: state.utility.loading.user,
    showMenu: state.utility.showMenu,
    router: state.router,
});

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        setUser,
        clearUser,

        addAllNotes,
        addAllCategories,

        setAllLoading,
        setErrorMsg,

        initialiseFilters,
    }, dispatch)
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps) (App));