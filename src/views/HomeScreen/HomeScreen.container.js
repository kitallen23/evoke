import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
    setErrorMsg,
    clearErrorMsg,
    setAddingNoteLoading,
    toggleNoteFilter,
    setSearchData,
} from "../../redux/utility/utility.actions";
import { addAllNotes, addNote, setMultiNoteVisibility } from "../../redux/notes/notes.actions";
import { syncFilters, toggleFilter, setAllFilters } from "../../redux/filter/filter.actions";

import HomeScreen from './HomeScreen';

const mapStateToProps = state => ({
    showMenu: state.utility.showMenu,
    showNoteFilter: state.utility.showNoteFilter,
    errorMsg: state.utility.errorMsg,
    notes: state.notes.notes,
    categories: state.categories.categories,
    filters: state.filter.filters,
    searchData: state.utility.searchData,
});

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        setErrorMsg,
        clearErrorMsg,

        addNote,
        addAllNotes,
        setAddingNoteLoading,
        setMultiNoteVisibility,

        toggleNoteFilter,
        syncFilters,
        toggleFilter,
        setAllFilters,

        setSearchData,
    }, dispatch)
};

export default connect(mapStateToProps, mapDispatchToProps) (HomeScreen);