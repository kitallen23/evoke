import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Collapse } from 'reactstrap';
import searchNotes from "../../utils/search";

import './HomeScreen.css';
import './quill.custom.css';

import TextEditor from '../../components/Editors/TextEditor';
import ListEditor from "../../components/Editors/ListEditor";
import TextNote from '../../components/Notes/TextNote';
import CategoryFilterItem from "../../components/CategoryFilterItem/CategoryFilterItem";

class HomeScreen extends Component {

    constructor( props ) {
        super( props );

        this.state = {
            blankCategoryCounter: 0,
            categories: [],
        };
    }

    componentDidMount() {
        let t_categories = this.sortCategories( this.props.categories );
        this.setState( {
            categories: t_categories
        }, () => this.populateCategoryCounters( this.state.categories, this.props.notes ) );

        this.props.syncFilters( this.props.categories );

        let filtered_notes = this.filterNotes( this.props.notes, this.props.filters );
        let searched_notes = this.searchNotes( filtered_notes, this.props.categories, this.props.searchData );

        this.props.setMultiNoteVisibility(
            searched_notes.map( note => {
                return {
                    _id: note._id,
                    display: note.display,
                }
            } )
        );
    }

    componentWillReceiveProps( nextProps ) {
        let organise_notes = false;

        // If filters changed, performSearch is set, or length of note array is different,
        // set organise notes to true
        if ( nextProps.filters !== this.props.filters ||
            nextProps.searchData.performSearch ||
            nextProps.notes.length !== this.props.notes.length ) {
            organise_notes = true;
        }

        // Re-apply search & filter if a note's category was changed
        nextProps.notes.forEach( note => {
            // Make sure note is in both arrays
            if ( this.props.notes.some( ( last_note ) => {
                    return last_note._id === note._id
                } ) ) {
                // Check category stored in each
                if ( this.props.notes.find( ( el ) => el._id === note._id ).category !== note.category ) {
                    organise_notes = true;
                }
            }
        } );

        if ( organise_notes ) {
            let filtered_notes = this.filterNotes( nextProps.notes, nextProps.filters );
            let searched_notes = this.searchNotes( filtered_notes, nextProps.categories, nextProps.searchData );

            this.props.setMultiNoteVisibility(
                searched_notes.map( note => {
                    return {
                        _id: note._id,
                        display: note.display,
                    }
                } )
            );

            this.props.setSearchData( {
                text: this.props.searchData.text,
                performSearch: false,
                isSearching: false,
            } );
        }

        if ( nextProps.notes.length !== this.props.notes.length ) {
            this.populateCategoryCounters( nextProps.categories, nextProps.notes );
        }
    }

    componentWillUnmount() {
        this.props.clearErrorMsg();
    }

    searchNotes( notes, categories, searchData ) {
        return searchNotes( notes, categories, searchData );
    }

    filterNotes( notes, filters ) {
        let filters_active = false;
        for ( const key of Object.keys( filters ) ) {
            if ( filters[ key ] ) {
                filters_active = true;
                break;
            }
        }

        if ( !filters_active ) {
            return notes.map( note => {
                return {
                    ...note,
                    display: true,
                }
            } );
        }

        let t_notes = [];
        notes.forEach( note => {
            if ( filters.hasOwnProperty( note.category ) ) {

                t_notes.push( {
                    ...note,
                    display: filters[ note.category ]
                } );
            }
        } );

        return t_notes;
    }

    sortCategories( categories ) {
        let t_categories = categories.slice();

        t_categories.sort( ( a, b ) => {
            const a_name = a.category.name.toUpperCase();
            const b_name = b.category.name.toUpperCase();

            let comparison = 0;
            if ( a_name > b_name ) {
                comparison = 1;
            } else if ( a_name < b_name ) {
                comparison = -1;
            }
            return comparison;
        } );

        return t_categories;
    }

    populateCategoryCounters( categories, notes ) {
        const noteCounters = [];
        let blankCategoryCounter = 0;

        // Populate note counters
        notes.forEach( note => {
            if ( typeof noteCounters[ note.category ] === "undefined" )
                noteCounters[ note.category ] = 1;
            else
                ++noteCounters[ note.category ];
        } );

        // Set counters in categories
        categories.forEach( category => {
            if ( typeof noteCounters[ category._id ] === "undefined" )
                category.noteCount = 0;
            else
                category.noteCount = noteCounters[ category._id ];
        } );

        // Set "no category" counter
        if ( typeof noteCounters[ "no_category" ] === "undefined" )
            blankCategoryCounter = 0;
        else
            blankCategoryCounter = noteCounters[ "no_category" ];

        this.setState( { categories: categories, blankCategoryCounter: blankCategoryCounter } );
        return categories;
    }

    toggleFilter( category_id ) {
        this.props.toggleFilter( category_id );
    }

    render() {

        const { filters, notes } = this.props;
        const { categories } = this.state;

        return (
            <div className="main-container">
                <div className="home-container" data-show={( this.props.showMenu ? "false" : "true" )}>

                    {/** Error message **/}
                    <Collapse isOpen={this.props.errorMsg.showErr}>
                        <div className="alert alert-danger alert-dismissible alert-msg">
                            <div className="close" onClick={() => this.props.clearErrorMsg()}
                                 style={{ cursor: 'pointer' }}>&times;</div>
                            <i className="fa fa-exclamation-triangle spacing-right" aria-hidden="true" />
                            <strong>Oops!</strong> {this.props.errorMsg.text}
                        </div>
                    </Collapse>

                    {/*Editor*/}
                    {/*TODO: Make this changeable somehow*/}
                    <TextEditor />
                    {/*<ListEditor />*/}

                    {/*Filter*/}
                    <div className={this.props.showNoteFilter ? "note-filter panel-shown" : "note-filter"}>
                        <div className="note-filter-header">
                            <div className="clickable"
                                 onClick={() => this.props.toggleNoteFilter()}>
                                <span className="spacing-right">tools</span>

                                <div className="filter-icon">
                                    <i className="fa fa-chevron-down" aria-hidden="true" />
                                </div>
                            </div>
                        </div>

                        <Collapse isOpen={this.props.showNoteFilter}>

                            {/*Sort*/}

                            {/*Filter*/}
                            <div className="filter-row">
                                <div className="filter-item-sm" onClick={() => this.props.setAllFilters( true )}>
                                    <span>select all</span>
                                </div>

                                <div className="filter-item-sm" onClick={() => this.props.setAllFilters( false )}>
                                    <span>clear all</span>
                                </div>
                            </div>

                            <CategoryFilterItem colour="#eeeeee"
                                                name="No category"
                                                selected={filters[ "no_category" ]}
                                                onClick={() => this.toggleFilter( "no_category" )}
                                                noteCount={this.state.blankCategoryCounter} />
                            {categories.map( ( category ) => (
                                <CategoryFilterItem key={category._id}
                                                    colour={category.category.colour.colourCode}
                                                    name={category.category.name}
                                                    selected={filters[ category._id ]}
                                                    onClick={() => this.toggleFilter( category._id )}
                                                    noteCount={category.noteCount} />
                            ) )}
                        </Collapse>
                    </div>

                    {/** Render notes **/}
                    <div>
                        {notes.map( ( note ) => (
                            <TextNote key={note._id} note={note} />
                        ) )}
                    </div>

                </div>
            </div>
        );
    }
}

HomeScreen.propTypes = {
    showMenu: PropTypes.bool,
    showNoteFilter: PropTypes.bool,
    errorMsg: PropTypes.object,
    notes: PropTypes.array,

    setErrorMsg: PropTypes.func,
    clearErrorMsg: PropTypes.func,

    addNote: PropTypes.func,
    addAllNotes: PropTypes.func,
    setAddingNoteLoading: PropTypes.func,

    toggleNoteFilter: PropTypes.func,
};

export default HomeScreen;
