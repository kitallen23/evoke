import React, { Component } from 'react';
import Firebase from './../../utils/Firebase';
import './WelcomeScreen.css';
import googleIcon from'../../img/btn_google_light_normal_ios.svg';

class WelcomeScreen extends Component {

    componentDidMount() {

    }

    render() {
        return (
            <div className="main-container">
                <div className="welcome-container">
                    <div className="evoke-title text-center"><i className="fa fa-i-cursor cursor-icon" aria-hidden="true" />evoke</div>
                    <p className="text-center">To get started using evoke, please sign in.</p>

                    <div className="signin-google-btn" onClick={() => Firebase.loginGoogle()}>
                        {/*<span className="signin-google-icon"></span>*/}
                        {/*<span className="signin-google-icon"></span>*/}
                        <img src={googleIcon} height="42px" width="42px" alt="Google"/>
                        <span>Sign in with Google</span>
                    </div>
                </div>
            </div>
        );
    }
}

export default WelcomeScreen;