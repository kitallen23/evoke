import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import {
    setErrorMsg,
    clearErrorMsg,
    setAddingCategoryLoading,
} from "../../redux/utility/utility.actions";
import {
    addCategory,
    setSelectedCategoryColour,
    resetSelectedCategoryColour,
    changeSortBy,
} from "../../redux/categories/categories.actions";

import CategoryScreen from './CategoryScreen';

const mapStateToProps = state => ({
    showMenu: state.utility.showMenu,
    errorMsg: state.utility.errorMsg,

    selectedColour: state.categories.selectedColour,
    addingCategory: state.utility.loading.addingCategory,
    sortBy: state.categories.sortBy,

    categories: state.categories.categories,
    notes: state.notes.notes,
});

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        setErrorMsg,
        clearErrorMsg,
        setAddingCategoryLoading,
        addCategory,
        setSelectedCategoryColour,
        resetSelectedCategoryColour,
        changeSortBy,
    }, dispatch)
};

export default connect(mapStateToProps, mapDispatchToProps) (CategoryScreen);
