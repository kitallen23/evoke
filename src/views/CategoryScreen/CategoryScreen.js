import React, { Component } from 'react';
import {
    Collapse,
    Dropdown,
    DropdownToggle,
    DropdownMenu
} from 'reactstrap';
import PropTypes from "prop-types";
import { CategoryAPI } from '../../utils/api';

import './CategoryScreen.css';
import CategoryItem from '../../components/CategoryItem';
import ColourPicker from '../../components/ColourPicker/ColourPicker';
import PaletteIcon from "../../components/ColourPicker/PaletteIcon/PaletteIcon";
import CircleSpinner from "../../components/utility/CircleSpinner/CircleSpinner";

class CategoryScreen extends Component {

    constructor( props ) {
        super( props );

        this.state = {
            categories: [],
            categoryText: "",
            sorterDropdownOpen: false,
        };

        this.toggleSorterDropdown = this.toggleSorterDropdown.bind( this );
        this.handleCategorySorterChange = this.handleCategorySorterChange.bind( this );
    }

    componentDidMount() {
        // Disable newlines in category input
        this.categoryInput.addEventListener( 'keypress', ( e ) => {
            if ( e.key === 'Enter' ) {
                e.preventDefault();
                this.submitCategory();
            }
        } );

        this.initialiseCategories();
    }

    componentWillUnmount() {
        this.props.clearErrorMsg();
    }

    componentWillReceiveProps( nextProps ) {
        // Trigger a sort
        if ( nextProps.sortBy !== this.props.sortBy ||
            nextProps.categories.length !== this.props.categories.length ) {
            this.sortCategories( nextProps.sortBy, nextProps.categories );
        }
    }

    initialiseCategories() {
        this.setState( { categories: this.props.categories },
            () => this.sortCategories( this.props.sortBy, this.props.categories ) );
    }

    sortCategories( sortBy, categories ) {

        this.populateCategoryCounters( categories );

        let t_categories = categories.slice();
        switch ( sortBy ) {
            case "name":
                t_categories.sort( ( a, b ) => {
                    const a_name = a.category.name.toUpperCase();
                    const b_name = b.category.name.toUpperCase();

                    let comparison = 0;
                    if ( a_name > b_name ) {
                        comparison = 1;
                    } else if ( a_name < b_name ) {
                        comparison = -1;
                    }
                    return comparison;
                } );
                break;
            case "date created":
                t_categories.sort( ( a, b ) => {
                    let a_dt = new Date( a.created_at );
                    let b_dt = new Date( b.created_at );

                    return b_dt - a_dt;
                } );
                break;
            case "size":
                t_categories.sort( ( a, b ) => {
                    return b.noteCount - a.noteCount;
                } );
                break;
            default:
        }
        this.setState( {
            categories: t_categories
        } );
    }

    populateCategoryCounters( categories ) {
        const noteCounters = [];

        // Populate note counters
        this.props.notes.forEach( note => {
            if ( typeof noteCounters[ note.category ] === "undefined" )
                noteCounters[ note.category ] = 1;
            else
                ++noteCounters[ note.category ];
        } );

        // Set counters in categories
        categories.forEach( category => {
            if ( typeof noteCounters[ category._id ] === "undefined" )
                category.noteCount = 0;
            else
                category.noteCount = noteCounters[ category._id ];
        } );

        this.setState( { categories: categories } );
    }

    handleCategorySorterChange( text ) {
        this.props.changeSortBy( text );
        this.toggleSorterDropdown();

        // TODO: Upload new selection to user in DB, don't bother reacting to response
    }

    handleCategoryTextChange() {
        this.setState( { categoryText: this.categoryInput.textContent } );
    }

    toggleSorterDropdown() {
        this.setState( { sorterDropdownOpen: !this.state.sorterDropdownOpen } );
    }

    submitCategory() {

        // Validation
        let text = this.state.categoryText.replace( /(\r\n|\n|\r)/g, "" ).trim();

        this.setState( { categoryText: text } );

        if ( text === "" ) {
            this.props.setErrorMsg( "Category name must not be left blank." );
            return;
        }

        let p = /^[a-z0-9\s'-]+$/i;
        if ( !text.match( p ) ) {
            this.props.setErrorMsg( "Category names must only include letters & numbers." );
            return;
        }

        if ( text.length > 60 ) {
            this.props.setErrorMsg( "Category name must be less than 60 characters long." );
            return;
        }

        this.props.setAddingCategoryLoading( true );

        // Create object to persist
        let category = {
            name: text,
            colour: this.props.selectedColour,
        };

        this.createCategory( category ).then( ( responseJson ) => {
            if ( responseJson.status === "SUCCESS" ) {
                this.props.addCategory( responseJson.category );

                // TODO: After submitting successfully:
                this.setState( { categoryText: "" } );
                this.props.resetSelectedCategoryColour();
                this.categoryInput.innerHTML = "";

                this.props.setAddingCategoryLoading( false );
            }
        } ).catch( ( err ) => {
            console.log( err );

            this.props.setErrorMsg( "Failed to create category. Please refresh the page and try again." );
            this.props.setAddingCategoryLoading( false );
        } )


    }

    createCategory = async ( category ) => {
        return await CategoryAPI.CreateCategory( category );
    };

    render() {

        // const { categories } = this.state;

        return (
            <div className="main-container">
                <div className="category-container" data-show={( this.props.showMenu ? "false" : "true" )}>

                    {/** Error message **/}
                    <Collapse isOpen={this.props.errorMsg.showErr}>
                        <div className="alert alert-danger alert-dismissible alert-msg">
                            <div className="close" onClick={() => this.props.clearErrorMsg()}
                                 style={{ cursor: 'pointer' }}>&times;</div>
                            <i className="fa fa-exclamation-triangle spacing-right" aria-hidden="true" />
                            <strong>Oops!</strong> {this.props.errorMsg.text}
                        </div>
                    </Collapse>

                    <div className="category-input-wrapper">
                        {/*Colour picker*/}
                        <ColourPicker selectedColour={this.props.selectedColour}
                                      onSelectColour={this.props.setSelectedCategoryColour.bind( this )}>
                            <div className="category-color-input-wrapper">
                                <PaletteIcon iconID="picker-label-colour"
                                             colour={this.props.selectedColour}
                                             showTooltip={false} />
                            </div>
                        </ColourPicker>

                        {/*Category name input*/}
                        <div
                            className={this.state.categoryText === "" ? "category-text-input blank" : "category-text-input"}
                            contentEditable="plaintext-only" data-placeholder="Add category..."
                            onInput={this.handleCategoryTextChange.bind( this )}
                            ref={( categoryInput ) => {
                                this.categoryInput = categoryInput;
                            }}
                        />

                        {/*Submit button*/}
                        {this.props.addingCategory ? (
                            <div className="category-submit-wrapper">
                                <CircleSpinner scale={1.4} />
                            </div>
                        ) : (
                            <div className="category-submit-wrapper" onClick={() => this.submitCategory()}>
                                <i className="fa fa-check" aria-hidden="true" />
                            </div>
                        )}
                    </div>

                    <div className="category-sorter-wrapper">
                        <Dropdown isOpen={this.state.sorterDropdownOpen} toggle={this.toggleSorterDropdown}>
                            <DropdownToggle tag="span">
                                <div className="category-sorter">
                                    <span className="spacing-right">Sort by:</span>
                                    <span className="spacing-right">{this.props.sortBy}</span>
                                    <i className="fa fa-chevron-down" aria-hidden="true" />
                                </div>
                            </DropdownToggle>

                            <DropdownMenu className="category-sorter-dropdown-list">
                                <div className="category-sorter-dropdown-item"
                                     onClick={() => this.handleCategorySorterChange( "date created" )}>date created
                                </div>
                                <div className="category-sorter-dropdown-item"
                                     onClick={() => this.handleCategorySorterChange( "name" )}>name
                                </div>
                                <div className="category-sorter-dropdown-item"
                                     onClick={() => this.handleCategorySorterChange( "size" )}>size
                                </div>
                            </DropdownMenu>
                        </Dropdown>
                    </div>

                    {/** Render categories **/}
                    {/*TODO: Let user know if they have no categories yet*/}
                    {this.state.categories.map( ( category ) => (
                        <CategoryItem key={category._id} category={category} noteCount={category.noteCount} />
                    ) )}

                </div>
            </div>
        );
    }
}

CategoryScreen.propTypes = {
    showMenu: PropTypes.bool,
    errorMsg: PropTypes.object,

    selectedColour: PropTypes.shape( {
        name: PropTypes.string,
        colourCode: PropTypes.string
    } ),

    addCategory: PropTypes.func,
    setErrorMsg: PropTypes.func,
    clearErrorMsg: PropTypes.func,
    changeSortBy: PropTypes.func,
};

export default CategoryScreen;