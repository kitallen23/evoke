import React, { Component } from 'react';
import './MenuLink.css';
import PropTypes from "prop-types";

import InlineHR from "../../utility/InlineHR/InlineHR";
import Firebase from "../../../utils/Firebase";

class MenuLink extends Component {

    constructor(props) {
        super(props);

        this.state = {
            width: '0'
        };

        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions() {
        this.setState({ width: window.innerWidth});
    }

    handleLinkClick( path ) {
        // Close menu if narrow menu is being displayed
        if(this.state.width <= 1160) {
            this.props.setMenu(false);
        }

        // Log out and navigate to welcome screen
        if ( path === '/logout' ) {
            Firebase.logout();
            this.props.push( '/welcome' );
            return;
        }
        this.props.push( path );
    }

    render() {
        return (
            <div className={this.props.path === this.props.currentPath ? "menu-link active" : "menu-link"}
                 onClick={() => this.handleLinkClick( this.props.path )}>
                <InlineHR color="rgba(255, 255, 255, 0.2)" margin={20} />
                {this.props.text}
                <InlineHR color="rgba(255, 255, 255, 0.2)" margin={20} />
            </div>
        );
    }
}

MenuLink.propTypes = {
    currentPath: PropTypes.string,
    path: PropTypes.string,
    text: PropTypes.string,

    push: PropTypes.func,
};

export default MenuLink;