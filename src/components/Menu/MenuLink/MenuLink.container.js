import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { push } from 'react-router-redux';

import MenuLink from './MenuLink';
import { setMenu } from "../../../redux/utility/utility.actions";

const mapStateToProps = state => ( {
    currentPath: state.router.location.pathname,
} );

const mapDispatchToProps = dispatch => {
    return bindActionCreators( { push, setMenu, }, dispatch )
};

export default connect( mapStateToProps, mapDispatchToProps )( MenuLink );