import React, { Component } from 'react';
import PropTypes from 'prop-types';
import firebase from 'firebase';

import './Menu.css';
import MenuLink from "./MenuLink";

class Menu extends Component {

    componentDidMount() {
        // TODO: Load history from database
    }

    logToken() {
        firebase.auth().currentUser.getIdToken(true).then((idToken) => {
            console.log(idToken);
        }).catch((error) => {
            console.log(error);
        });
    }

    render() {
        return (
            <div>
                <div className="menu-wrapper" data-shown={ this.props.showMenu ? "show" : "hide" }>

                    <MenuLink text="Notes" path="/" />
                    <MenuLink text="Manage Categories" path="/categories" />
                    <MenuLink text="Log out" path="/logout" />

                    {/*TO BE REMOVED. Only here for test purposes.*/}
                    {/*<button onClick={() => this.logToken()}>Log token</button>*/}

                </div>

                <div className="menu-spacer" data-shown={ this.props.showMenu ? "show" : "hide" } />

            </div>
        );
    }
}

Menu.propTypes = {
    showMenu: PropTypes.bool,
    location: PropTypes.string,

    setMenu: PropTypes.func,
    push: PropTypes.func,
};

export default Menu;