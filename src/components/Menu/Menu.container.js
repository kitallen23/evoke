import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setMenu } from "../../redux/utility/utility.actions";
import { push } from 'react-router-redux';

import Menu from './Menu';

const mapStateToProps = state => ({
    showMenu: state.utility.showMenu,
    location: state.router.location.pathname,
});

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        setMenu,
        push,
    }, dispatch)
};

export default connect(mapStateToProps, mapDispatchToProps) (Menu);