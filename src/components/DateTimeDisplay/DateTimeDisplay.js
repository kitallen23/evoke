import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as moment from "moment/moment";

class DateTimeDisplay extends Component {

    constructor( props ) {
        super( props );

        this.state = {
            dtText: "just now",
        };
    }

    componentDidMount() {
        this.generateDisplayTime( this.props.createdAt, this.props.updatedAt );
        this.timer = setInterval( () => {
            this.generateDisplayTime( this.props.createdAt, this.props.updatedAt )
        }, 30000 );
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.updatedAt !== this.props.updatedAt )
            this.generateDisplayTime( nextProps.createdAt, nextProps.updatedAt );
    }

    componentWillUnmount() {
        clearInterval( this.timer );
    }

    generateDisplayTime( createdAt, updatedAt ) {
        let str = "";
        let ts = "";
        if ( updatedAt === "" ) {
            str = "created ";
            ts = moment( createdAt );
        }
        else {
            str = "updated ";
            ts = moment( updatedAt );
        }

        // In the last minute
        let minuteAgo = moment().subtract( 1, 'minute' );
        if ( ts.isAfter( minuteAgo, 'second' ) ) {
            this.setState( { dtText: str + "just now" } );
            return;
        }

        // In the last hour
        let hourAgo = moment().subtract( 1, 'hour' );
        if ( ts.isAfter( hourAgo, 'second' ) ) {
            let difference = moment().diff( ts, 'minutes' );
            if ( difference <= 1 )
                this.setState( { dtText: str + difference + " minute ago" } );
            else
                this.setState( { dtText: str + difference + " minutes ago" } );
            return;
        }

        // In the last day
        let startOfToday = moment().startOf( 'day' );
        if ( ts.isAfter( startOfToday, 'second' ) ) {
            let difference = moment().diff( ts, 'hours' );
            if ( difference <= 1 )
                this.setState( { dtText: str + difference + " hour ago" } );
            else
                this.setState( { dtText: str + difference + " hours ago" } );
            return;
        }

        // Before today
        this.setState( { dtText: str + ts.format( "Do MMM" ) } )
    }

    render() {
        return (
            <div>{this.state.dtText}</div>
        );
    }
}

DateTimeDisplay.propTypes = {
    createdAt: PropTypes.string.isRequired,
    updatedAt: PropTypes.string.isRequired,
};

export default DateTimeDisplay;
