import { connect } from 'react-redux';

import SearchBar from './SearchBar';
import { bindActionCreators } from "redux";
// import { setErrorMsg } from "../../../redux/utility/utility.actions";
import { setSearchData, setSearchingStatus } from "../../redux/utility/utility.actions";

const mapStateToProps = state => ({
    searchData: state.utility.searchData,
});

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        setSearchData,
        setSearchingStatus,
    }, dispatch)
};

export default connect(mapStateToProps, mapDispatchToProps) (SearchBar);
