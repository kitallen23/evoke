import React, { Component } from 'react';
import './SearchBar.css';
import PropTypes from "prop-types";
import CircleSpinner from "../utility/CircleSpinner/CircleSpinner";

class SearchBar extends Component {

    constructor( props ) {
        super( props );

        this.state = {
            t_searchText: "",
        };

        this.searchCallback = null;
    }

    componentDidMount() {
        // Disable newlines in search input
        this.searchInput.addEventListener( 'keypress', ( e ) => {
            if ( e.key === 'Enter' ) {
                e.preventDefault();
            }
        } );
    }

    handleSearchTextChange() {
        const searchText = this.searchInput.textContent;
        console.log( "Search text:" );
        console.log( searchText );

        this.setState( { t_searchText: searchText } );
        this.props.setSearchData( {
            text: searchText,
            performSearch: false,
            isSearching: true,
        } );

        // Clear previous callback if necessary
        if ( this.searchCallback ) clearTimeout( this.searchCallback );

        // Set new callback
        this.searchCallback = setTimeout( () => this.initiateSearch(),
            searchText === "" ? 0 : 600 );
    }

    initiateSearch() {
        this.props.setSearchData( {
            text: this.state.t_searchText,
            performSearch: true,
            isSearching: true,
        } );
    }

    clearSearchText() {
        this.searchInput.innerHTML = "";
        this.setState( { t_searchText: "" } );
        this.handleSearchTextChange();
    }

    render() {

        const { searchData } = this.props;

        return (
            <div className="search-bar-wrapper">
                <div>
                    <i className="fa fa-search" aria-hidden="true" />
                </div>
                <div
                    className={this.state.t_searchText === "" ? "search-text-input blank" : "search-text-input"}
                    contentEditable="plaintext-only" data-placeholder="Search notes..."
                    onInput={this.handleSearchTextChange.bind( this )}
                    ref={( searchInput ) => {
                        this.searchInput = searchInput;
                    }}
                />
                {searchData.isSearching && <div className="end-cap"><CircleSpinner scale={0.6} /></div>}

                {!searchData.isSearching && this.state.t_searchText !== "" && (
                    <div className="end-cap" onClick={() => this.clearSearchText()}>
                        <i className="fa fa-times" aria-hidden="true" />
                    </div>
                )}
            </div>
        );
    }
}

SearchBar.propTypes = {
    searchText: PropTypes.string.isRequired,
};

export default SearchBar;