import React, { Component } from 'react';
import './Spinner.css';
import PropTypes from 'prop-types';

class Spinner extends Component {
    render() {
        return (
            <div className="spinner"
                 style={{
                     zoom: this.props.scale,
                     MozTransform: 'scale(' + this.props.scale + ')'
                 }}
            >
                <div className="rect1" />
                <div className="rect2" />
                <div className="rect3" />
                <div className="rect4" />
                <div className="rect5" />
                <div className="rect6" />
            </div>
        );
    }
}

Spinner.propTypes = {
    scale: PropTypes.number,
};
Spinner.defaultProps = {
    scale: 1,
};

export default Spinner;