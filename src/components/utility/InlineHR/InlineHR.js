import React, { Component } from 'react';
// import './InlineHR.css';
import PropTypes from 'prop-types';

class InlineHR extends Component {
    render() {
        return (
            <div style={{
                display: 'inline-block',
                width: this.props.width,
                height: '1px',
                background: this.props.color,
                margin: this.props.margin,
            }}>

            </div>
        );
    }
}

InlineHR.propTypes = {
    color: PropTypes.string,
    width: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    margin: PropTypes.number,
};
InlineHR.defaultProps = {
    color: '#eeeeee',
    width: '100%',
    margin: 10,
};

export default InlineHR;