import React, { Component } from 'react';
import './CircleSpinner.css';
import PropTypes from 'prop-types';

class CircleSpinner extends Component {
    render() {
        return (
            <div style={{ transform: 'scale(' + this.props.scale + ')' }}>
                <div className="loader" />
            </div>
        );
    }
}

CircleSpinner.propTypes = {
    scale: PropTypes.number,
};
CircleSpinner.defaultProps = {
    scale: 1,
};

export default CircleSpinner;