import React, { Component } from 'react';
import './MenuButton.css';
import PropTypes from 'prop-types';

class MenuButton extends Component {

    render() {
        return (
            <div className="nav-menu-button-wrapper" onClick={this.props.onClick}>
                {this.props.open ? (
                    <div id="nav-menu-button" className="open">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                ) : (
                    <div id="nav-menu-button">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                )}
            </div>
        );
    }
}

MenuButton.propTypes = {
    onClick: PropTypes.func,
    open: PropTypes.bool,
};

export default MenuButton;