import React, { Component } from 'react';
import {
    Dropdown,
    DropdownToggle,
    DropdownMenu,
} from 'reactstrap';
import PropTypes from "prop-types";

import './CategoryPicker.css';

class CategoryPicker extends Component {

    constructor( props ) {
        super( props );

        this.state = {
            categories: [],
            dropdownOpen: false,
        };

        this.toggleDropdown = this.toggleDropdown.bind( this );
        this.onCategoryClick = this.onCategoryClick.bind( this );
    }

    componentDidMount() {
        this.setState( { categories: this.props.categories } )
    }

    toggleDropdown() {
        this.setState( { dropdownOpen: !this.state.dropdownOpen } );
    }

    onCategoryClick( category ) {
        this.toggleDropdown();
        this.props.onSelectCategory( category );
    }

    render() {

        const { categories } = this.state;
        const { blankCategory } = this.props;

        return (
            <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggleDropdown}>
                <DropdownToggle tag="span">
                    {this.props.children}
                </DropdownToggle>

                <DropdownMenu className="category-picker-dropdown-list">
                    <div className="category-picker-dropdown-menu-wrapper">
                        <div className="category-picker-item"
                             onClick={() => this.onCategoryClick( blankCategory )}>
                            <i className="fa fa-circle spacing-right" aria-hidden="true"
                               style={{
                                   color: '#eeeeee',
                                   fontSize: '1.2em'
                               }} />
                            {"No category"}
                        </div>
                        {categories.map( ( category ) => (
                            <div className="category-picker-item"
                                 key={category._id}
                                 onClick={() => this.onCategoryClick( category )}>
                                <i className="fa fa-circle spacing-right" aria-hidden="true"
                                   style={{
                                       color: category.category.colour.colourCode,
                                       fontSize: '1.2em'
                                   }} />
                                {category.category.name}
                            </div>
                        ) )}
                    </div>
                </DropdownMenu>
            </Dropdown>
        );
    }
}

CategoryPicker.propTypes = {
    onSelectCategory: PropTypes.func.isRequired,
};
CategoryPicker.defaultProps = {};

export default CategoryPicker;