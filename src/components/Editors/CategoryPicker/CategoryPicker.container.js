import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
// import { setErrorMsg, setAddingNoteLoading } from "../../../redux/utility/utility.actions";

import CategoryPicker from './CategoryPicker';

const mapStateToProps = state => ({
    categories: state.categories.categories,
    blankCategory: state.categories.blankCategory,
});

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
    }, dispatch)
};

export default connect(mapStateToProps, mapDispatchToProps) (CategoryPicker);