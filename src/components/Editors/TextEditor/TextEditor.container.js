import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setErrorMsg, setAddingNoteLoading } from "../../../redux/utility/utility.actions";
import { addNote } from "../../../redux/notes/notes.actions";

import TextEditor from './TextEditor';

const mapStateToProps = state => ({
    addingNote: state.utility.loading.addingNote,
    blankCategory: state.categories.blankCategory,
});

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        setErrorMsg,
        addNote,
        setAddingNoteLoading,
    }, dispatch)
};

export default connect(mapStateToProps, mapDispatchToProps) (TextEditor);