import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NoteAPI } from '../../../utils/api';
import ReactQuill from 'react-quill';
import { quillFormats, quillModules } from "../../../utils/Quill";

import './TextEditor.css';
import CircleSpinner from "../../utility/CircleSpinner/CircleSpinner";
import PaletteIcon from "../../ColourPicker/PaletteIcon/PaletteIcon";
import CategoryPicker from "../CategoryPicker";

class TextEditor extends Component {

    constructor( props ) {
        super( props );

        this.state = {
            titleText: "",
            editorText: "",
            category: this.props.blankCategory,
        };

        this.quillRef = null;      // Quill instance
        this.reactQuillRef = null; // ReactQuill component
        this.handleTextChange = this.handleTextChange.bind(this);
    }

    // bindings = {
    //     submitOnEnter: {
    //         key: 'enter',
    //         shortKey: true,
    //         handler: () => {
    //             this.handleCreateNote();
    //         }
    //     },
    // };
    //
    // componentWillMount() {
    //     quillModules.keyboard = {
    //         bindings: this.bindings,
    //     }
    // }

    componentDidMount() {
        this.attachQuillRefs();
        document.querySelector( '.ql-editor' ).tabIndex = 2;

        // Disable newlines in title input
        this.titleInput.addEventListener( 'keypress', ( e ) => {
            if ( e.key === 'Enter' ) {
                e.preventDefault();
                this.reactQuillRef.focus();
            }
        } );
    }

    attachQuillRefs = () => {
        if ( typeof this.reactQuillRef.getEditor !== 'function' ) return;
        this.quillRef = this.reactQuillRef.getEditor();
    };

    handleTitleChange() {
        this.setState( { titleText: this.titleInput.innerHTML } );
    }

    handleTextChange( text ) {
        this.setState( { editorText: text } );
    }

    handleCategoryClicked( category ) {
        this.setState( { category: category } );
    }

    handleCreateNote() {

        if ( this.quillRef.getText().trim().length === 0 ) {
            this.props.setErrorMsg( "Note text must not be left blank." );
            return;
        }

        this.props.setAddingNoteLoading( true );

        // Create note object
        let note = {
            title: this.state.titleText.trim(),
            text: this.state.editorText.trim(),
        };

        // Call API to create note
        this.createNote( note ).then( ( responseJson ) => {
            if ( responseJson.status === "SUCCESS" ) {
                // Add note to redux store to virtually 'sync' it with db
                console.log( 'Create note response: ', responseJson.note );
                this.props.addNote( responseJson.note );

                // Clear the text editor
                this.setState( {
                    titleText: "",
                    editorText: "",
                    category: this.props.blankCategory,
                } );
                this.titleInput.innerHTML = "";
            }
            else {
                this.props.setErrorMsg( responseJson.message );
            }
            this.props.setAddingNoteLoading( false );
        } ).catch( ( err ) => {
            console.log( err );

            // Display error and stop loading spinner
            this.props.setErrorMsg( "Failed to add note. Please refresh the page and try again." );
            this.props.setAddingNoteLoading( false );
        } );

    }

    createNote = async ( note ) => {
        return await NoteAPI.CreateTextNote( note, this.state.category._id );
    };

    render() {
        return (
            <div>
                <div className="editor-wrapper">

                    <div className={this.state.titleText === "" ? "editor-title-input blank" : "editor-title-input"}
                         contentEditable="plaintext-only" data-placeholder="Title"
                         onInput={this.handleTitleChange.bind( this )}
                         ref={( titleInput ) => {
                             this.titleInput = titleInput;
                         }}
                         tabIndex={1}
                    />

                    <div>
                        <ReactQuill
                            value={this.state.editorText || ''}
                            onChange={this.handleTextChange}
                            modules={quillModules}
                            formats={quillFormats}
                            placeholder={"Write a note..."}
                            ref={( el ) => {
                                this.reactQuillRef = el
                            }}
                        />
                    </div>

                    <div className="editor-bottom-panel">
                        <div className="editor-bottom-item">
                            {/*Category picker*/}
                            <CategoryPicker onSelectCategory={( category ) => this.handleCategoryClicked( category )}>
                                <div className="category-picker-input-wrapper">
                                    <div className="colour-picker">
                                        <PaletteIcon iconID="category-picker-colour"
                                                     colour={this.state.category.category.colour}
                                                     showTooltip={false} />
                                    </div>
                                    {this.state.category._id !== "no_category" ? (
                                        <div className="category-picker-label spacing-left">
                                            {this.state.category.category.name}
                                        </div>
                                    ) : (

                                        <div className="category-picker-assign-label spacing-left">
                                            assign category
                                        </div>
                                    )}
                                </div>
                            </CategoryPicker>
                        </div>

                        <div className="editor-bottom-item">
                            {/*Submit button*/}
                            {this.props.addingNote ? (
                                <div className="submit-btn-wrapper">
                                    <div className="submit-btn">
                                        <CircleSpinner scale={1.4} />
                                    </div>
                                    <div className="submit-label">saving...</div>
                                </div>
                            ) : (
                                <div className="submit-btn-wrapper">
                                    <div className="submit-btn" onClick={() => this.handleCreateNote()}>
                                        <i className="fa fa-check" aria-hidden="true" />
                                    </div>
                                    <div className="submit-label">save</div>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

TextEditor.propTypes = {
    addingNote: PropTypes.bool,

    setErrorMsg: PropTypes.func,
    addNote: PropTypes.func,
    setAddingNoteLoading: PropTypes.func,
};

export default TextEditor;