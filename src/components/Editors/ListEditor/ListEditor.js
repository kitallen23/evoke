import React, { Component } from 'react';
import { NoteAPI } from '../../../utils/api';

import '../TextEditor/TextEditor.css';
import CircleSpinner from "../../utility/CircleSpinner/CircleSpinner";
import PaletteIcon from "../../ColourPicker/PaletteIcon/PaletteIcon";
import CategoryPicker from "../CategoryPicker";
import ListItemManager from "../../ListItemManager/ListItemManager";

class ListEditor extends Component {

    constructor( props ) {
        super( props );

        this.state = {
            titleText: "",
            category: this.props.blankCategory,
            listItems: [
                [
                    {
                        _id: 1,
                        text: "",
                        isChecked: false,
                        type: "listitem",
                        parent: 0,
                    }
                ]
            ],
        };

    }

    componentDidMount() {
    }

    handleTitleChange() {
        this.setState( { titleText: this.titleInput.innerHTML } );
    }

    handleListChange( listItems ) {
        this.setState( { listItems: listItems } );
    }

    handleCategoryClicked( category ) {
        this.setState( { category: category } );
    }

    handleCreateNote() {

        // TODO: Check for blank note, return if blank

        // TODO: Uncomment
        // this.props.setAddingNoteLoading( true );

        // TODO: Create note object

        // TODO: Call API to create note
        // this.createNote( note ).then( ( responseJson ) => {
        //     if ( responseJson.status === "SUCCESS" ) {
        //         // Add note to redux store to virtually 'sync' it with db
        //         console.log( 'Create note response: ', responseJson.note );
        //         this.props.addNote( responseJson.note );
        //
        //         // Clear the text editor
        //         this.setState( {
        //             titleText: "",
        //             // editorText: "",
        //             category: this.props.blankCategory,
        //         } );
        //         this.titleInput.innerHTML = "";
        //     }
        //     else {
        //         this.props.setErrorMsg( responseJson.message );
        //     }
        //     this.props.setAddingNoteLoading( false );
        // } ).catch( ( err ) => {
        //     console.log( err );
        //
        //     // Display error and stop loading spinner
        //     this.props.setErrorMsg( "Failed to add note. Please refresh the page and try again." );
        //     this.props.setAddingNoteLoading( false );
        // } );

    }

    createNote = async ( note ) => {
        return await NoteAPI.CreateTextNote( note, this.state.category._id );
    };

    render() {
        return (
            <div>
                <div className="editor-wrapper">

                    <div className={this.state.titleText === "" ? "editor-title-input blank" : "editor-title-input"}
                         contentEditable="plaintext-only" data-placeholder="Title"
                         onInput={this.handleTitleChange.bind( this )}
                         ref={( titleInput ) => {
                             this.titleInput = titleInput;
                         }}
                         tabIndex={1}
                    />

                    <ListItemManager listItems={this.state.listItems}
                                     onChange={this.handleListChange.bind(this)} />

                    <div className="editor-bottom-panel">
                        <div className="editor-bottom-item">
                            {/*Category picker*/}
                            <CategoryPicker onSelectCategory={( category ) => this.handleCategoryClicked( category )}>
                                <div className="category-picker-input-wrapper">
                                    <div className="colour-picker">
                                        <PaletteIcon iconID="category-picker-colour"
                                                     colour={this.state.category.category.colour}
                                                     showTooltip={false} />
                                    </div>
                                    {this.state.category._id !== "no_category" ? (
                                        <div className="category-picker-label spacing-left">
                                            {this.state.category.category.name}
                                        </div>
                                    ) : (

                                        <div className="category-picker-assign-label spacing-left">
                                            assign category
                                        </div>
                                    )}
                                </div>
                            </CategoryPicker>
                        </div>

                        <div className="editor-bottom-item">
                            {/*Submit button*/}
                            {this.props.addingNote ? (
                                <div className="submit-btn-wrapper">
                                    <div className="submit-btn">
                                        <CircleSpinner scale={1.4} />
                                    </div>
                                    <div className="submit-label">saving...</div>
                                </div>
                            ) : (
                                <div className="submit-btn-wrapper">
                                    <div className="submit-btn" onClick={() => this.handleCreateNote()}>
                                        <i className="fa fa-check" aria-hidden="true" />
                                    </div>
                                    <div className="submit-label">save</div>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ListEditor;