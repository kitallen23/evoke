import React, { Component } from 'react';

import ListItem from './ListItem/ListItem';

class ListItemManager extends Component {

    constructor( props ) {
        super( props );

        this.state = {
            listItems: [
                [
                    {
                        _id: 24,
                        text: "item 24, this should go above all subheadings",
                        isChecked: false,
                        type: "listitem",
                        parent: 0,
                    },
                    {
                        _id: 3,
                        text: "How did it go?",
                        isChecked: false,
                        type: "listitem",
                        parent: 0,
                    },
                ],
                [
                    {
                        _id: 2,
                        text: "BANANAS",
                        isChecked: true,
                        type: "subheading",
                        parent: 0,
                    },
                    {
                        _id: 6,
                        text: "item 6 (B)",
                        isChecked: false,
                        type: "listitem",
                        parent: 2,
                    },
                    {
                        _id: 9,
                        text: "item 9 (B)",
                        isChecked: false,
                        type: "listitem",
                        parent: 2,
                    },
                    {
                        _id: 10,
                        text: "item 10 (B)",
                        isChecked: false,
                        type: "listitem",
                        parent: 2,
                    },
                ],
                [
                    {
                        _id: 1,
                        text: "APPLES",
                        isChecked: false,
                        type: "subheading",
                        parent: 0,
                    },
                    {
                        _id: 18,
                        text: "item 18 (A)",
                        isChecked: false,
                        type: "listitem",
                        parent: 1,
                    },
                    {
                        _id: 7,
                        text: "item 7 (A)",
                        isChecked: false,
                        type: "listitem",
                        parent: 1,
                    },
                ],
            ],
        };
    }

    componentDidMount() {

        // TODO: If list passed via PROPS is blank, initialise a new one,
        // TODO: ensuring the 0th group is included

        let listItems = this.sortListItems( this.props.listItems );
        listItems = this.addBlankListItems( listItems, this.getHighestIndex( listItems ) );

        // this.setState( {
        //     listItems: listItems,
        // } );
        this.props.onChange( listItems );
    }

    // TODO: Fix
    // TODO: Don't delete if subheading with items attached
    onDelete( itemID ) {
        // let t_listItems = this.props.listItems.slice();
        // if ( this.props.listItems.length > 1 ) {
        //
        //     // Ensure we're not deleting the last item in the list
        //     if ( t_listItems[ t_listItems.length - 1 ]._id !== itemID ) {
        //         t_listItems = this.removeListItemByID( t_listItems, itemID );
        //     }
        // }
        //
        // t_listItems = this.addBlankListItems( t_listItems );
        //
        // // this.setState( { listItems: t_listItems } );
        // this.props.onChange( t_listItems );
    }

    // TODO: Fix
    removeListItemByID( listItems, id ) {
        return listItems.filter( listItem => listItem._id !== id );
    }

    onTextChange( listItem ) {
        let t_listItems = this.props.listItems.slice();

        // Find and modify the list item
        for ( let i = 0; i < t_listItems.length; ++i ) {
            let t_itemGroup = t_listItems[ i ];

            for ( let j = 0; j < t_itemGroup.length; ++j ) {
                if ( t_itemGroup[ j ]._id === listItem._id ) {
                    t_itemGroup[ j ].text = listItem.text;
                    break;
                }
            }
        }

        // Ensure blank list items are present
        t_listItems = this.addBlankListItems( t_listItems, this.getHighestIndex( t_listItems ) );
        t_listItems = this.sortListItems( t_listItems );

        // this.setState( { listItems: t_listItems } );
        this.props.onChange( t_listItems );
    }

    onCheckboxClicked( itemID ) {
        let t_listItems = this.props.listItems.slice();

        for ( let i = 0; i < t_listItems.length; ++i ) {
            let t_itemGroup = t_listItems[ i ];

            for ( let j = 0; j < t_itemGroup.length; ++j ) {
                if ( t_itemGroup[ j ]._id === itemID ) {
                    t_itemGroup[ j ].isChecked = !t_itemGroup[ j ].isChecked;
                    break;
                }
            }
        }

        // Sort array
        this.sortListItems( t_listItems );

        // this.setState( { listItems: t_listItems } );
        this.props.onChange( t_listItems );
    }

    // TODO: Change this, it needs to now change the location of the subheading
    // and reorder the list
    onTypeChange( itemID, type ) {
        let t_listItems = this.props.listItems.slice();

        for ( let i = 0; i < t_listItems.length; ++i ) {
            let t_itemGroup = t_listItems[ i ];

            for ( let j = 0; j < t_itemGroup.length; ++j ) {
                if ( t_itemGroup[ j ]._id === itemID ) {
                    t_itemGroup[ j ].type = type;
                    t_itemGroup[ j ].isChecked = false;
                    break;
                }
            }
        }

        // this.setState( { listItems: t_listItems } );
        this.props.onChange( t_listItems );
    }

    // TODO: Fix
    addBlankListItems( listItems, currIndex ) {

        let t_index = currIndex;
        let t_listItems = [];

        // Add a blank item at the end of each sub-heading (including 0)
        listItems.forEach( listItemGroup => {
            // Create a copy of each list item group
            let t_listItemGroup = listItemGroup.slice();

            if ( listItemGroup.length > 0 ) {
                // Get the parent ID of this group
                let p_id = listItemGroup[ 0 ].type === "subheading" ?
                    listItemGroup[ 0 ]._id : listItemGroup[ 0 ].parent;

                // Add an empty item to the list group
                if ( listItemGroup[ listItemGroup.length - 1 ].text !== "" ) {
                    t_listItemGroup.push( {
                        _id: t_index + 1,
                        text: "",
                        isChecked: false,
                        type: "listitem",
                        parent: p_id,
                    } );
                }

                ++t_index;
                t_listItems.push( t_listItemGroup );
            }
        } );

        return t_listItems;
    }

    getHighestIndex( listItems ) {
        let m_index = -1;
        listItems.forEach( itemGroup => {
            itemGroup.forEach( item => {
                if ( item._id > m_index ) {
                    m_index = item._id;
                }
            } );
        } );
        console.log( 'Highest index: ', m_index );
        return m_index;
    }

    sortListItems( listItems ) {

        let t_listItems = [];

        // Sort item groups
        let t_itemGroups = listItems.sort( ( a, b ) => {
            if ( a.length > 0 && b.length > 0 ) {
                let a_id = a[ 0 ].type === "subheading" ? a[ 0 ]._id : a[ 0 ].parent;
                let b_id = b[ 0 ].type === "subheading" ? b[ 0 ]._id : b[ 0 ].parent;

                return a_id - b_id;
            }
            // This should never happen
            return false;
        } );

        // Sort items within each item group
        t_itemGroups.forEach( item_grp => {
            let t_item_grp = [];
            // Generate lists of checked and unchecked items, sort them by ID,
            // and add them to the new list
            let t_unchecked = item_grp.filter( item => {
                return !item.isChecked && item.type !== "subheading";
            } ).sort( ( a, b ) => {
                return a._id - b._id;
            } );
            let t_checked = item_grp.filter( item => {
                return item.isChecked && item.type !== "subheading";
            } ).sort( ( a, b ) => {
                return a._id - b._id;
            } );
            let t_subheading = item_grp.find( item => {
                return item.type === "subheading";
            } );

            if ( typeof t_subheading !== "undefined" )
                t_item_grp.push( t_subheading );

            // Add new item group to listItems
            t_listItems.push( t_item_grp.concat( t_unchecked ).concat( t_checked ) );
        } );

        // Return new listItems
        console.log( t_listItems );
        return t_listItems;
    }

    render() {

        const { listItems } = this.props;

        return (

            <div>
                {listItems.map( listItemGroup => (

                    listItemGroup.map( listItem => (
                        <ListItem key={listItem._id}
                                  id={listItem._id}
                                  text={listItem.text}
                                  isChecked={listItem.isChecked}
                                  type={listItem.type}
                                  onTextChange={this.onTextChange.bind( this )}
                                  onCheckboxClicked={this.onCheckboxClicked.bind( this )}
                                  onTypeChange={this.onTypeChange.bind( this )}
                                  onDelete={this.onDelete.bind( this )} />
                    ) )

                ) )}
            </div>

        );
    }
}

export default ListItemManager;
