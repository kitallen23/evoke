import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Dropdown, DropdownToggle, DropdownMenu } from 'reactstrap';

import './ListItem.css';

class ListItem extends Component {

    constructor( props ) {
        super( props );

        this.state = {
            dropdownOpen: false,
        };

        this.toggleDropdown = this.toggleDropdown.bind( this );
    }

    componentDidMount() {
        // Set initial text
        this.textInput.innerHTML = this.props.text;

        // Delete this item on focusout if blank
        this.textInput.addEventListener( 'focusout', () => {
            if ( this.props.text === "" ) {
                this.props.onDelete( this.props.id );
            }
        } );
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.text === this.props.text ) {
            this.textInput.innerHTML = nextProps.text;
        }
    }

    handleTextChange() {
        this.props.onTextChange( {
            _id: this.props.id,
            text: this.textInput.textContent,
            isChecked: this.props.isChecked,
        } );
    }

    handleCheckboxClicked() {
        this.props.onCheckboxClicked( this.props.id );
    }

    handleDeleteClicked() {
        this.props.onDelete( this.props.id );
    }

    handleTypeChangeClicked() {
        this.props.onTypeChange( this.props.id, this.props.type === "subheading" ? "listitem" : "subheading" );
        this.toggleDropdown();
    }

    toggleDropdown() {
        this.setState( { dropdownOpen: !this.state.dropdownOpen } );
    }

    render() {

        return (
            <div className="list-item-wrapper">
                <div className="list-item">
                    {/*Checkbox etc.*/}
                    {this.props.type === "listitem" ? (
                        <div className="left-header-wrapper">
                            {this.props.text !== "" ? (
                                <div className={this.props.isChecked ? "tickbox checked" : "tickbox"}
                                     onClick={this.handleCheckboxClicked.bind( this )}>
                                    <i className="fa fa-check" />
                                </div>
                            ) : (
                                <div className="add-icon">
                                    <i className="fa fa-plus" />
                                </div>
                            )}
                        </div>
                    ) : (
                        <div />
                        //{/*<div className="left-header-wrapper blank">*/}
                        //{/*/!*<i className="fa fa-hashtag" />*!/*/}
                        //{/*</div>*/}
                    )}

                    {/*Textbox*/}
                    <div className={"textbox" + ( this.props.type === "subheading" ? " subheading" : "" ) + (this.props.text === "" ? " blank" : "")}
                         contentEditable="plaintext-only"
                         data-placeholder={this.props.type === "subheading" ? "Add sub-heading" : "Add list item"}
                         onInput={this.handleTextChange.bind( this )}
                         ref={( textInput ) => {
                             this.textInput = textInput;
                         }}
                         style={{ display: !this.props.isChecked || this.props.type === "subheading" ? "flex" : "none" }}
                         spellCheck="false"
                    />
                    <div className="text-display"
                         style={{ display: this.props.isChecked && this.props.type !== "subheading" ? "flex" : "none" }}>
                        {this.props.text}
                    </div>

                    {/*End buttons*/}
                    {this.props.text !== "" && (
                        <div className="btn-wrapper">
                            <div className="end-btn" onClick={this.handleDeleteClicked.bind( this )}>
                                <i className="fa fa-times-circle" />
                            </div>


                            <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggleDropdown}>
                                <DropdownToggle tag="span">
                                    <div className="end-btn">
                                        <i className="fa fa-ellipsis-v" />
                                    </div>
                                </DropdownToggle>
                                <DropdownMenu className="dropdown-list" right>
                                    <div className="dropdown-item" onClick={this.handleTypeChangeClicked.bind( this )}>
                                        {this.props.type === "subheading" ? "Convert to list item" : "Convert to sub-heading"}
                                    </div>
                                    {this.props.type !== "subheading" && (
                                        <div className="dropdown-item">
                                            {/*TODO: Change this wording, as it can be added to base parent or a subheading*/}
                                            Assign to sub-heading...
                                        </div>
                                    )}
                                </DropdownMenu>
                            </Dropdown>
                        </div>
                    )}
                </div>
                <div className="bottom-border" />
            </div>
        );
    }
}

ListItem.propTypes = {
    id: PropTypes.number.isRequired,
    text: PropTypes.string.isRequired,
    isChecked: PropTypes.bool.isRequired,
    type: PropTypes.string.isRequired,

    onTextChange: PropTypes.func.isRequired,
    onCheckboxClicked: PropTypes.func.isRequired,
    onTypeChange: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
};

export default ListItem;
