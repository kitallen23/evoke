import { connect } from 'react-redux';

import TextNote from './TextNote';
import { bindActionCreators } from "redux";
import { clearAnimateOnEntry, removeNote, updateNote } from "../../../redux/notes/notes.actions";
import { setErrorMsg } from "../../../redux/utility/utility.actions";

const mapStateToProps = state => ({
    notes: state.notes.notes,
    blankCategory: state.categories.blankCategory,
    categories: state.categories.categories,
});

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        clearAnimateOnEntry,
        setErrorMsg,
        removeNote,
        updateNote,
    }, dispatch)
};

export default connect(mapStateToProps, mapDispatchToProps) (TextNote);
