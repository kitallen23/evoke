import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { UncontrolledTooltip, Collapse } from 'reactstrap';
import { NoteAPI } from '../../../utils/api';
import ReactQuill from 'react-quill';
import { quillFormats, quillModules } from "../../../utils/Quill";

import './TextNote.css';
import CategoryPicker from '../../Editors/CategoryPicker';
import PaletteIcon from '../../ColourPicker/PaletteIcon/PaletteIcon';
import CircleSpinner from '../../utility/CircleSpinner/CircleSpinner';
import DateTimeDisplay from "../../DateTimeDisplay/DateTimeDisplay";

class TextNote extends Component {

    constructor( props ) {
        super( props );

        this.state = {
            category: this.props.blankCategory,

            titleText: "",
            editText: "",

            savingCategory: false,
            savingNote: false,

            isEdited: false,

            errorMsg: {
                showErr: false,
                text: "",
            }
        };

        this.animate = null;
        this.autosaveCallback = null;
        this.quillRef = null;      // Quill instance
        this.reactQuillRef = null; // ReactQuill component
        this.handleTextChange = this.handleTextChange.bind(this);
    }

    componentDidMount() {
        this.attachQuillRefs();
        this.setState( {
            editText: this.props.note.note,
            titleText: this.props.note.title,
        } );

        if ( this.props.note.animateOnEntry ) {
            this.expandOnMount();
            this.props.clearAnimateOnEntry( this.props.note._id );
        }

        // Disable newlines in title input
        if ( this.props.note.title !== "" ) {
            this.titleInput.addEventListener( 'keypress', ( e ) => {
                if ( e.key === 'Enter' ) {
                    e.preventDefault();
                    this.reactQuillRef.focus();
                }
            } );

            // Set initial title text
            this.titleInput.innerHTML = this.props.note.title;
        }

        // Find related category
        let found = false;
        this.props.categories.forEach( ( category ) => {
            if ( category._id === this.props.note.category ) {
                this.setState( { category: category } );
                found = true;
            }
        } );
        if ( !found ) {
            this.setState( { category: this.props.blankCategory } );
        }
    }

    componentWillReceiveProps( nextProps ) {
        if ( nextProps.note !== this.props.note ) {
            // Find related category
            let found = false;
            nextProps.categories.forEach( ( category ) => {
                if ( category._id === nextProps.note.category ) {
                    this.setState( { category: category } );
                    found = true;
                }
            } );
            if ( !found ) {
                this.setState( { category: this.props.blankCategory } );
            }

            // Set edit text & check for changes
            this.handleTextChange( nextProps.note.note );
            this.props.note.title !== "" && this.handleTitleChange( nextProps.note.title );

            if ( nextProps.note.title !== "" ) {
                // Set title text
                this.titleInput.innerHTML = nextProps.note.title;
            }
        }
    }

    componentWillUnmount() {
        console.log("Unmounting");
        clearInterval( this.timer );
        if ( this.autosaveCallback ) {
            this.autosave();
            clearTimeout( this.autosaveCallback );
        }
    }

    attachQuillRefs = () => {
        if ( typeof this.reactQuillRef.getEditor !== 'function' ) return;
        this.quillRef = this.reactQuillRef.getEditor();
    };

    // Refer to:
    // https://css-tricks.com/using-css-transitions-auto-dimensions/
    expandOnMount() {
        let el = this.animate;
        el.style.height = '0';
        el.style.overflow = 'hidden';

        // get the height of the element's inner content, regardless of its actual size
        let sectionHeight = el.scrollHeight;
        // console.log( sectionHeight );

        // have the element transition to the height of its inner content
        el.style.height = sectionHeight + 'px';

        // when the next css transition finishes (which should be the one we just triggered)
        el.addEventListener( 'transitionend', function removeTransition() {

            // remove this event listener so it only gets triggered once
            el.removeEventListener( 'transitionend', removeTransition );

            // remove "height" from the element's inline styles, so it can return to its initial value
            el.style.height = null;
            el.style.overflow = null;
        } );
    }

    collapseOnDelete() {
        let el = this.animate;
        let _h = el.scrollHeight;
        el.style.overflow = 'hidden';

        let elementTransition = el.style.transition;
        el.style.transition = '';

        requestAnimationFrame( () => {
            el.style.height = _h + 'px';
            el.style.transition = elementTransition;

            // on the next frame (as soon as the previous style change has taken effect),
            // have the element transition to height: 0
            requestAnimationFrame( () => {
                el.style.height = 0 + 'px';
            } );
        } );

        let removeNote = this.props.removeNote.bind( this );
        let noteID = this.props.note._id;

        // when the next css transition finishes (which should be the one we just triggered)
        el.addEventListener( 'transitionend', function removeTransition() {

            // remove this event listener so it only gets triggered once
            el.removeEventListener( 'transitionend', removeTransition );

            // Remove this note from redux store
            removeNote( noteID );
        } );
    }

    handleTextChange( newText ) {
        // Clear autosave callback handle
        if ( this.autosaveCallback ) clearTimeout( this.autosaveCallback );

        const isEdited = !this.editorTextIsEqual( newText, this.props.note.note );
        this.setState( {
            editText: newText,
            isEdited: isEdited,
        } );

        // If edited, set autosave for 3s from now
        if ( isEdited )
            this.autosaveCallback = setTimeout( () => this.autosave(), 3000 );
    }

    handleTitleChange() {
        // Clear autosave callback handle
        if ( this.autosaveCallback ) clearTimeout( this.autosaveCallback );

        const isEdited = ( this.titleInput.innerHTML !== this.props.note.title );

        this.setState( {
            titleText: this.titleInput.innerHTML,
            isEdited: isEdited,
        } );

        // If edited, set autosave for 3s from now
        if ( isEdited )
            this.autosaveCallback = setTimeout( () => this.autosave(), 3000 );
    }

    editorTextIsEqual( html_a, html_b ) {
        // Create temporary elements
        let a = document.createElement( "html" );
        let b = document.createElement( "html" );

        // Populate temporary elements
        a.innerHTML = html_a;
        b.innerHTML = html_b;

        // Find <pre> children
        let code_a = a.getElementsByTagName( "pre" );
        let code_b = b.getElementsByTagName( "pre" );

        // Remove <pre> elements from the temporary elements
        for ( let i = 0; i < code_a.length; ++i ) {
            code_a[ i ].parentNode.removeChild( code_a[ i ] );
        }
        for ( let i = 0; i < code_b.length; ++i ) {
            code_b[ i ].parentNode.removeChild( code_b[ i ] );
        }

        // Check equality of all contents except <pre>
        if ( this.strip( a.innerHTML ) !== this.strip( b.innerHTML ) ) return false;
        if ( a.innerHTML.length !== b.innerHTML.length ) return false;

        // Re-populate elements for further tests
        a.innerHTML = html_a;
        b.innerHTML = html_b;
        code_a = a.getElementsByTagName( "pre" );
        code_b = b.getElementsByTagName( "pre" );

        // Check equality of <pre> contents
        if ( code_a.length !== code_b.length ) return false;
        for ( let i = 0; i < code_a.length; ++i ) {
            if ( this.strip( code_a[ i ].innerHTML ) !== this.strip( code_b[ i ].innerHTML ) ) return false;
        }

        // All tests passed
        return true;
    }

    autosave() {
        // Return & don't autosave if note text is empty
        if ( this.strip( this.state.editText ).trim().length === 0 ) {
            return;
        }

        this.setState( { savingNote: true }, () => {

            let t_note = Object.assign( {}, this.props.note );
            t_note.note = this.state.editText;
            t_note.title = this.state.titleText;

            this.persistUpdateNote( this.props.note._id, t_note ).then( ( responseJson ) => {

                if ( responseJson.status === "SUCCESS" ) {
                    // Check no changes have been made since autosave began
                    const isTextEdited = !this.editorTextIsEqual( responseJson.note.note, this.state.editText );
                    const isTitleEdited = ( responseJson.note.title !== this.titleInput.innerHTML );

                    if ( !isTextEdited && !isTitleEdited )
                        this.props.updateNote( responseJson.note );
                }
                this.setState( { savingNote: false } );

            } ).catch( ( err ) => {
                console.log( err );
                this.setState( { savingNote: false } );
            } );
        } );
    }

    strip( html ) {
        let tmp = document.createElement( "DIV" );
        tmp.innerHTML = html;
        return tmp.textContent || tmp.innerText;
    }

    handleDeleteNote() {
        this.collapseOnDelete();

        this.deleteNote( this.props.note._id ).then( ( responseJson ) => {
            if ( responseJson.status !== "SUCCESS" ) {
                this.props.setErrorMsg( responseJson.message + ". Please refresh the page and try again." );
            }
        } ).catch( ( err ) => {
            console.log( err );
            this.props.setErrorMsg( "Failed to delete note. Please refresh the page and try again." );
        } );
    }

    handleCategoryClicked( category ) {
        this.setState( { savingCategory: true }, () => {

            let t_note = Object.assign( {}, this.props.note );
            t_note.note = this.state.editText;
            t_note.title = this.state.titleText;
            t_note.category = category._id;

            this.persistUpdateNote( this.props.note._id, t_note ).then( ( responseJson ) => {
                if ( responseJson.status !== "SUCCESS" ) {
                    this.props.setErrorMsg( responseJson.message + ". Please refresh the page and try again." );
                }
                else {
                    this.props.updateNote( {
                        ...responseJson.note,
                        display: t_note.display,
                    } );
                }
                this.setState( { savingCategory: false } );

            } ).catch( ( err ) => {
                console.log( err );
                this.props.setErrorMsg( "Failed to update note. Please refresh the page and try again." );
                this.setState( { savingCategory: false } );
            } );
        } );
    }

    handleSaveClicked() {
        // Clear autosave callback handle
        if ( this.autosaveCallback ) clearTimeout( this.autosaveCallback );

        if ( this.strip( this.state.editText ).trim().length === 0 ) {
            this.setErrorMsg( "Note text must not be left blank." );
            return;
        }

        this.setState( { savingNote: true }, () => {

            let t_note = Object.assign( {}, this.props.note );
            t_note.note = this.state.editText;
            t_note.title = this.state.titleText;

            this.persistUpdateNote( this.props.note._id, t_note ).then( ( responseJson ) => {
                if ( responseJson.status !== "SUCCESS" ) {
                    this.props.setErrorMsg( responseJson.message + ". Please refresh the page and try again." );
                }
                else {
                    this.props.updateNote( responseJson.note );
                }
                this.setState( { savingNote: false } );

            } ).catch( ( err ) => {
                console.log( err );
                this.props.setErrorMsg( "Failed to update note. Please refresh the page and try again." );
                this.setState( { savingNote: false } );
            } );
        } );
    }

    deleteNote = async ( id ) => {
        return await NoteAPI.DeleteNote( id );
    };

    persistUpdateNote = async ( id, note ) => {
        return await NoteAPI.UpdateNote( id, note );
    };

    clearErrorMsg() {
        this.setState( {
            errorMsg: {
                showErr: false,
                text: "",
            }
        } );
    }

    setErrorMsg( text ) {
        this.setState( {
            errorMsg: {
                showErr: true,
                text: text,
            }
        } )
    }

    render() {
        const { note } = this.props;

        return (
            <div className="animate" tabIndex={-1}
                 ref={( animate ) => {
                     this.animate = animate;
                 }}>

                <div className="hide-wrapper" data-display={note.display ? "true" : "false"}>

                    <div className={"note-err"}>
                        <Collapse isOpen={this.state.errorMsg.showErr}>
                            <div className="alert alert-danger alert-dismissible alert-msg">
                                <div className="close" onClick={() => this.clearErrorMsg()}
                                     style={{ cursor: 'pointer' }}>&times;</div>
                                <i className="fa fa-exclamation-triangle spacing-right" aria-hidden="true" />
                                <strong>Oops!</strong> {this.state.errorMsg.text}
                            </div>
                        </Collapse>
                    </div>

                    <div className="note-wrapper" tabIndex={-1}>

                        <div className={this.state.titleText === "" ? "note-title-input blank" : "note-title-input"}
                             contentEditable="plaintext-only" data-placeholder="Title"
                             onInput={this.handleTitleChange.bind( this )}
                             ref={( titleInput ) => {
                                 this.titleInput = titleInput;
                             }}
                             data-empty={this.state.titleText === "" ? "true" : "false"}
                        />

                        <div className="text-note-editor" tabIndex={-1}>
                            <ReactQuill
                                value={this.state.editText || ''}
                                onChange={this.handleTextChange}
                                modules={quillModules}
                                formats={quillFormats}
                                placeholder={"Write a note..."}
                                ref={( el ) => {
                                    this.reactQuillRef = el
                                }}
                            />
                        </div>

                        <div className="note-bottom-panel">

                            <div className="note-bottom-item">
                                {/*Category picker*/}
                                {this.state.savingCategory ? (
                                    <div className="loading-wrapper">
                                        <CircleSpinner scale={1.5} />
                                    </div>
                                ) : (
                                    <CategoryPicker
                                        onSelectCategory={( category ) => this.handleCategoryClicked( category )}>
                                        <div className="category-picker-input-wrapper">
                                            <div className="category-picker">
                                                <div className="zoom-me">
                                                    <PaletteIcon iconID="category-picker-colour"
                                                                 colour={this.state.category.category.colour}
                                                                 showTooltip={false} />
                                                </div>
                                            </div>
                                            {this.state.category._id !== "no_category" ? (
                                                <div className="category-picker-label spacing-left">
                                                    {this.state.category.category.name}
                                                </div>
                                            ) : (
                                                <div className="category-picker-assign-label spacing-left">
                                                    assign category
                                                </div>
                                            )}
                                        </div>
                                    </CategoryPicker>
                                )}
                            </div>

                            {/*Time display*/}
                            <div className="note-bottom-item timestamp-text hidden">
                                <DateTimeDisplay createdAt={note.created_at} updatedAt={typeof note.updated_at === "undefined" ? "" : note.updated_at} />
                            </div>

                            {this.state.savingNote && (
                                <div className="note-bottom-item">
                                    <div className="loading-wrapper">
                                        <CircleSpinner scale={1.5} />
                                    </div>
                                </div>
                            )}
                            {!this.state.savingNote && this.state.isEdited && (
                                <div className="note-bottom-item">
                                    <div className="note-bottom-button save"
                                         onClick={() => this.handleSaveClicked()}>
                                        done
                                        <i className="fa fa-check" aria-hidden="true" />
                                    </div>
                                </div>
                            )}

                            {!this.state.savingNote && !this.state.isEdited && (
                                <div className="note-bottom-item hidden">

                                    <UncontrolledTooltip placement="bottom" target={"tt-edit" + note._id}
                                                         delay={{ show: 500, hide: 0 }}>
                                        Edit
                                    </UncontrolledTooltip>
                                    <div className="note-bottom-icon"
                                         id={"tt-edit" + note._id}>
                                        {/*onClick={() => this.doSomethingLikeToggleUpdate()}>*/}
                                        <i className="fa fa-pencil" aria-hidden="true" />
                                    </div>

                                    <UncontrolledTooltip placement="bottom" target={"tt-delete" + note._id}
                                                         delay={{ show: 500, hide: 0 }}>
                                        Delete
                                    </UncontrolledTooltip>
                                    <div className="note-bottom-icon"
                                         id={"tt-delete" + note._id}
                                         onClick={() => this.handleDeleteNote()}>
                                        <i className="fa fa-trash" aria-hidden="true" />
                                    </div>

                                </div>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

TextNote.propTypes = {
    note: PropTypes.object,

    clearAnimateOnEntry: PropTypes.func,
    removeNote: PropTypes.func,
    setErrorMsg: PropTypes.func,
};

export default TextNote;
