import React, { Component } from 'react';
import {
    Dropdown,
    DropdownToggle,
    DropdownMenu,
} from 'reactstrap';
import PropTypes from "prop-types";

import './ColourPicker.css';
import PaletteIcon from "./PaletteIcon/PaletteIcon";

class ColourPicker extends Component {

    constructor( props ) {
        super( props );

        this.state = {
            dropdownOpen: false,
        }

        this.toggleDropdown = this.toggleDropdown.bind( this );
        this.onColourClick = this.onColourClick.bind( this );
    }

    toggleDropdown() {
        this.setState( { dropdownOpen: !this.state.dropdownOpen } );
    }

    onColourClick( colour ) {
        this.toggleDropdown();
        this.props.onSelectColour( colour );
    }

    render() {
        return (
            <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggleDropdown}>
                <DropdownToggle tag="span">
                    {this.props.children}
                </DropdownToggle>

                <DropdownMenu className="colour-picker-dropdown-list">
                    <div className="colour-picker-dropdown-menu-wrapper">
                        <div className="colour-row">
                            <PaletteIcon colour={{ name: "Red", colourCode: "#FF4136" }}
                                         selected={this.props.selectedColour.colourCode === "#FF4136"}
                                         iconID={"pi-FF4136" + this.props.idPrefix}
                                         onClick={this.onColourClick} />
                            <PaletteIcon colour={{ name: "Olive", colourCode: "#3D9970" }}
                                         selected={this.props.selectedColour.colourCode === "#3D9970"}
                                         iconID={"pi-3D9970" + this.props.idPrefix}
                                         onClick={this.onColourClick} />
                            <PaletteIcon colour={{ name: "Blue", colourCode: "#0074D9" }}
                                         selected={this.props.selectedColour.colourCode === "#0074D9"}
                                         iconID={"pi-0074D9" + this.props.idPrefix}
                                         onClick={this.onColourClick} />
                            <PaletteIcon colour={{ name: "Maroon", colourCode: "#85144b" }}
                                         selected={this.props.selectedColour.colourCode === "#85144b"}
                                         iconID={"pi-85144b" + this.props.idPrefix}
                                         onClick={this.onColourClick} />
                        </div>
                        <div className="colour-row">
                            <PaletteIcon colour={{ name: "Orange", colourCode: "#FF851B" }}
                                         selected={this.props.selectedColour.colourCode === "#FF851B"}
                                         iconID={"pi-FF851B" + this.props.idPrefix}
                                         onClick={this.onColourClick} />
                            <PaletteIcon colour={{ name: "Green", colourCode: "#2ECC40" }}
                                         selected={this.props.selectedColour.colourCode === "#2ECC40"}
                                         iconID={"pi-2ECC40" + this.props.idPrefix}
                                         onClick={this.onColourClick} />
                            <PaletteIcon colour={{ name: "Teal", colourCode: "#39CCCC" }}
                                         selected={this.props.selectedColour.colourCode === "#39CCCC"}
                                         iconID={"pi-39CCCC" + this.props.idPrefix}
                                         onClick={this.onColourClick} />
                            <PaletteIcon colour={{ name: "Purple", colourCode: "#B10DC9" }}
                                         selected={this.props.selectedColour.colourCode === "#B10DC9"}
                                         iconID={"pi-B10DC9" + this.props.idPrefix}
                                         onClick={this.onColourClick} />
                        </div>
                        <div className="colour-row">
                            <PaletteIcon colour={{ name: "Yellow", colourCode: "#FFDC00" }}
                                         selected={this.props.selectedColour.colourCode === "#FFDC00"}
                                         iconID={"pi-FFDC00" + this.props.idPrefix}
                                         onClick={this.onColourClick} />
                            <PaletteIcon colour={{ name: "Lime", colourCode: "#01FF70" }}
                                         selected={this.props.selectedColour.colourCode === "#01FF70"}
                                         iconID={"pi-01FF70" + this.props.idPrefix}
                                         onClick={this.onColourClick} />
                            <PaletteIcon colour={{ name: "Aqua", colourCode: "#7FDBFF" }}
                                         selected={this.props.selectedColour.colourCode === "#7FDBFF"}
                                         iconID={"pi-7FDBFF" + this.props.idPrefix}
                                         onClick={this.onColourClick} />
                            <PaletteIcon colour={{ name: "Fuchsia", colourCode: "#F012BE" }}
                                         selected={this.props.selectedColour.colourCode === "#F012BE"}
                                         iconID={"pi-F012BE" + this.props.idPrefix}
                                         onClick={this.onColourClick} />
                        </div>
                    </div>
                </DropdownMenu>
            </Dropdown>
        );
    }
}

ColourPicker.propTypes = {
    children: PropTypes.node.isRequired,
    selectedColour: PropTypes.object,
    idPrefix: PropTypes.string,

    onSelectColour: PropTypes.func,
};
ColourPicker.defaultProps = {
    idPrefix: "",
};

export default ColourPicker;