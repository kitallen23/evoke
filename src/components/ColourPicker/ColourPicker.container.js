import { connect } from 'react-redux';

import ColourPicker from './ColourPicker';

const mapStateToProps = state => ( {
    selectedColour: state.categories.selectedColour,
} );

export default connect( mapStateToProps )( ColourPicker );
