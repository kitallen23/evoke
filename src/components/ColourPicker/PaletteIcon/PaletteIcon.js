import React, { Component } from 'react';
import PropTypes from "prop-types";
import { UncontrolledTooltip } from 'reactstrap';

import './PaletteIcon.css';

// Colours taken from: http://clrs.cc/
class PaletteIcon extends Component {

    handleOnClick() {
        this.props.onClick(this.props.colour);
    }

    render() {
        return (
            <div>
                {this.props.showTooltip && (
                    <UncontrolledTooltip placement="top" target={this.props.iconID} delay={{show: 500, hide: 100}}>
                        {this.props.colour.name}
                    </UncontrolledTooltip>
                )}

                {!!this.props.onClick ? (
                    <div className={this.props.selected ? "palette-icon selected" : "palette-icon"}
                         id={this.props.iconID}
                         style={{background: this.props.colour.colourCode}}
                         onClick={() => this.handleOnClick()}>
                        { this.props.selected && <i className="fa fa-check" aria-hidden="true" />}
                    </div>
                ) : (
                    <div className={this.props.selected ? "palette-icon selected" : "palette-icon"}
                         id={this.props.iconID}
                         style={{background: this.props.colour.colourCode}}>
                        { this.props.selected && <i className="fa fa-check" aria-hidden="true" />}
                    </div>
                )}
            </div>
        );
    }
}

PaletteIcon.propTypes = {
    iconID: PropTypes.string.isRequired,
    colour: PropTypes.shape({
        name: PropTypes.string.isRequired,
        colourCode: PropTypes.string.isRequired
    }),
    selected: PropTypes.bool,
    showTooltip: PropTypes.bool,

    onClick: PropTypes.func,
};
PaletteIcon.defaultProps = {
    selected: false,
    showTooltip: true,
};

export default PaletteIcon;