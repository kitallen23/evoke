import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setMenu, toggleMenu } from "../../redux/utility/utility.actions";
import { push } from 'react-router-redux';

import NavBar from './NavBar';

const mapStateToProps = state => ({
    showMenu: state.utility.showMenu,
    user: state.auth.user,
});

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        setMenu,
        toggleMenu,
        push,
    }, dispatch)
};

export default connect(mapStateToProps, mapDispatchToProps) (NavBar);
