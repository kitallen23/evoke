import React, { Component } from 'react';
import './NavBar.css';
import PropTypes from 'prop-types';
import { UncontrolledDropdown, DropdownToggle, DropdownMenu } from 'reactstrap';
import Firebase from './../../utils/Firebase';

import SearchBar from "../SearchBar";
import MenuButton from '../MenuButton/MenuButton';

class NavBar extends Component {

    // constructor(props) {
    //     super(props);
    // }

    render() {
        return (
            <div>
                <div className="navbar-wrapper">
                    <div className="navbar">
                        <div className="nav-item nav-left">
                            {this.props.isActive ? (
                                <MenuButton
                                    open={this.props.showMenu}
                                    onClick={() => this.props.toggleMenu()}
                                />
                            ) : (
                                <MenuButton
                                    open={false}
                                />
                            )}

                            {/*Search*/}
                            <div className="search-wrapper">
                                <SearchBar searchText="" />
                            </div>
                        </div>

                        <div className="nav-item nav-center">
                            <div className="nav-brand" onClick={() => this.props.push( '/' )}>evoke</div>
                        </div>

                        <div className="nav-item nav-right">
                            {this.props.user !== null && (
                                <UncontrolledDropdown>
                                    <DropdownToggle tag="span">
                                        <div className="user-info">
                                            <span className="user-text">{this.props.user.displayName}</span>
                                            <img src={this.props.user.photoURL} className="user-avatar" alt="Avatar" />
                                        </div>
                                    </DropdownToggle>
                                    <DropdownMenu className="dropdown-list" right>
                                        <div className="dropdown-item" onClick={() => Firebase.logout()}>Log out</div>
                                    </DropdownMenu>
                                </UncontrolledDropdown>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

NavBar.propTypes = {
    push: PropTypes.func,
    setMenu: PropTypes.func,

    showMenu: PropTypes.bool,
    isActive: PropTypes.bool,
};

export default NavBar;