import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { updateCategory, clearAnimateOnEntry, removeCategory } from "../../redux/categories/categories.actions";
import { setErrorMsg } from "../../redux/utility/utility.actions";

import CategoryItem from './CategoryItem';

const mapStateToProps = state => ( {} );

const mapDispatchToProps = dispatch => {
    return bindActionCreators( {
        updateCategory,
        setErrorMsg,
        clearAnimateOnEntry,
        removeCategory,
    }, dispatch )
};

export default connect( mapStateToProps, mapDispatchToProps )( CategoryItem );
