import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './CategoryItem.css';
import { UncontrolledTooltip } from 'reactstrap';
import { CategoryAPI } from "../../utils/api";

import PaletteIcon from '../ColourPicker/PaletteIcon/PaletteIcon';
import ColourPicker from '../ColourPicker/ColourPicker';
import CircleSpinner from "../../components/utility/CircleSpinner/CircleSpinner";

class CategoryItem extends Component {

    constructor( props ) {
        super( props );

        this.state = {
            showPanel: false,
            updatingColour: false,
            updatingName: false,
            editable: false,
            categoryText: "",
        };

    }

    componentDidMount() {
        if ( this.props.category.animateOnEntry ) {
            this.expandOnMount();
            this.props.clearAnimateOnEntry( this.props.category._id );
        }

        this.categoryInput.addEventListener( 'keypress', ( e ) => {
            if ( e.key === 'Enter' ) {
                e.preventDefault();
                this.handleUpdateName( this.state.categoryText );
            }
        } );
    }

    // Refer to:
    // https://css-tricks.com/using-css-transitions-auto-dimensions/
    expandOnMount() {
        let el = this.animate;
        el.style.height = '0';
        el.style.overflow = 'hidden';

        // get the height of the element's inner content, regardless of its actual size
        let sectionHeight = el.scrollHeight;

        // have the element transition to the height of its inner content
        el.style.height = sectionHeight + 'px';

        // when the next css transition finishes (which should be the one we just triggered)
        el.addEventListener( 'transitionend', function removeTransition() {

            // remove this event listener so it only gets triggered once
            el.removeEventListener( 'transitionend', removeTransition );

            // remove "height" from the element's inline styles, so it can return to its initial value
            el.style.height = null;
            el.style.overflow = null;
        } );
    }

    collapseOnDelete() {
        let el = this.animate;
        let _h = el.scrollHeight;
        el.style.overflow = 'hidden';

        let elementTransition = el.style.transition;
        el.style.transition = '';

        requestAnimationFrame( () => {
            el.style.height = _h + 'px';
            el.style.transition = elementTransition;

            // on the next frame (as soon as the previous style change has taken effect),
            // have the element transition to height: 0
            requestAnimationFrame( () => {
                el.style.height = 0 + 'px';
            } );
        } );

        let removeCategory = this.props.removeCategory.bind( this );
        let categoryID = this.props.category._id;

        // when the next css transition finishes (which should be the one we just triggered)
        el.addEventListener( 'transitionend', function removeTransition() {

            // remove this event listener so it only gets triggered once
            el.removeEventListener( 'transitionend', removeTransition );

            // Remove this note from redux store
            removeCategory( categoryID );
        } );
    }

    togglePanel() {
        this.setState( { showPanel: !this.state.showPanel } );
    }

    toggleEditable() {

        if ( !this.state.editable ) {
            this.categoryInput.innerHTML = this.props.category.category.name;
            this.setState( { showPanel: false } );

            let range = document.createRange();
            range.selectNodeContents( this.categoryInput );
            let sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange( range );
            setTimeout( () => {
                this.categoryInput.focus();
            } );
        }

        this.setState( {
            editable: !this.state.editable,
            categoryText: this.props.category.category.name
        } );
    }

    handleCategoryTextChange() {
        this.setState( { categoryText: this.categoryInput.textContent } );
    }

    handleDeleteCategory() {
        this.collapseOnDelete();

        this.deleteCategory( this.props.category._id ).then( ( responseJson ) => {
            if ( responseJson.status !== "SUCCESS" ) {
                this.props.setErrorMsg( responseJson.message + ". Please refresh the page and try again." );
            }
        } ).catch( ( err ) => {
            console.log( err );
            this.props.setErrorMsg( "Failed to delete category. Please refresh the page and try again." );
        } );
    }

    handleUpdateColour( colour ) {

        this.setState( { updatingColour: true }, function () {

            let t_cat = Object.assign( {}, this.props.category );
            t_cat.category.colour = colour;

            this.persistUpdateCategory( t_cat ).then( ( resp ) => {
                if ( resp.status !== "SUCCESS" ) {
                    this.props.setErrorMsg( resp.message + ". Please refresh the page and try again." );
                }
                else {
                    this.props.updateCategory( resp.category );
                }
                this.setState( { updatingColour: false } );

            } ).catch( err => {

                console.log( err );
                this.props.setErrorMsg( "Failed to contact the server. Please refresh the page and try again." );
                this.setState( { updatingColour: false } );
            } )

        } );
    }

    handleUpdateName( name ) {
        let text = name.replace( /(\r\n|\n|\r)/g, "" ).trim();
        this.setState( { categoryText: text } );

        // Return if no changes need to be made
        if ( name === this.props.category.category.name ) {
            this.toggleEditable();
            return;
        }
        if ( text === "" ) {
            this.props.setErrorMsg( "Category name must not be left blank." );
            return;
        }

        let p = /^[a-z0-9\s]+$/i;
        if ( !text.match( p ) ) {
            this.props.setErrorMsg( "Category names must only include letters & numbers." );
            return;
        }

        if ( text.length > 60 ) {
            this.props.setErrorMsg( "Category name must be less than 60 characters long." );
            return;
        }

        this.setState( { updatingName: true }, function () {
            let t_cat = this.props.category;
            t_cat.category.name = text;

            this.persistUpdateCategory( t_cat ).then( ( resp ) => {
                if ( resp.status !== "SUCCESS" ) {
                    this.props.setErrorMsg( resp.message + ". Please refresh the page and try again." );
                }
                else {
                    this.props.updateCategory( resp.category );
                }
                this.setState( { updatingName: false } );
                this.toggleEditable();

            } ).catch( err => {

                console.log( err );
                this.props.setErrorMsg( "Failed to contact the server. Please refresh the page and try again." );
                this.setState( { updatingName: false } );
                this.toggleEditable();
            } )

        } );
    }

    deleteCategory = async ( id ) => {
        return await CategoryAPI.DeleteCategory( id );
    };

    persistUpdateCategory = async ( category ) => {
        return await CategoryAPI.UpdateCategory( category._id, category );
    };

    render() {

        return (
            <div className="category-item-container" ref={( animate ) => {
                this.animate = animate;
            }}>
                <div className="category-item">

                    {/*Colour picker*/}
                    {this.state.updatingColour ? (
                        <div
                            className={this.state.showPanel ? "category-item-color-wrapper panelShown" : "category-item-color-wrapper"}>
                            <CircleSpinner scale={1.4} />
                        </div>
                    ) : (
                        <ColourPicker selectedColour={this.props.category.category.colour}
                                      onSelectColour={this.handleUpdateColour.bind( this )}
                                      idPrefix={this.props.category._id}>
                            <div
                                className={this.state.showPanel ? "category-item-color-wrapper panelShown" : "category-item-color-wrapper"}>
                                <PaletteIcon iconID={"label-colour-" + this.props.category._id}
                                             colour={this.props.category.category.colour}
                                             showTooltip={false} />
                            </div>
                        </ColourPicker>
                    )}

                    {/*Name*/}
                    <div
                        className={this.state.categoryText === "" ? "category-item-name-wrapper editable blank" : "category-item-name-wrapper editable"}
                        contentEditable="plaintext-only" data-placeholder="Category name..."
                        onInput={this.handleCategoryTextChange.bind( this )}
                        ref={( categoryInput ) => {
                            this.categoryInput = categoryInput;
                        }}
                        style={{ display: this.state.editable ? "flex" : "none" }}
                    />

                    <div className="category-item-name-wrapper"
                         style={{ display: this.state.editable ? "none" : "flex" }}>
                        {this.props.category.category.name}
                    </div>

                    {/*Save/discard options*/}
                    <div className="btn-row-wrapper"
                         style={{ display: this.state.editable && !this.state.updatingName ? "flex" : "none" }}>

                        <UncontrolledTooltip placement="bottom" target={"tt-discard" + this.props.category._id}
                                             delay={{ show: 500, hide: 0 }}>
                            Discard changes
                        </UncontrolledTooltip>
                        <div className="category-icon hover-warning"
                             id={"tt-discard" + this.props.category._id}
                             onClick={() => this.toggleEditable()}>
                            <i className="fa fa-times" aria-hidden="true" />
                        </div>

                        <UncontrolledTooltip placement="bottom" target={"tt-save" + this.props.category._id}
                                             delay={{ show: 500, hide: 0 }}>
                            Save changes
                        </UncontrolledTooltip>
                        <div
                            className={this.state.showPanel ? "category-icon hover-success panelShown" : "category-icon hover-success"}
                            id={"tt-save" + this.props.category._id}
                            onClick={() => this.handleUpdateName( this.state.categoryText )}>
                            <i className="fa fa-check" aria-hidden="true" />
                        </div>
                    </div>
                    <div className="btn-row-wrapper"
                         style={{ display: this.state.updatingName ? "flex" : "none" }}>
                        <div className="category-icon">
                            <CircleSpinner scale={1.4} />
                        </div>
                    </div>

                    <span style={{ display: this.state.editable ? "none" : "inline" }}>
                        notes: {this.props.category.noteCount}
                    </span>

                    {/*Normal options*/}
                    <div className="btn-row-wrapper"
                         style={{ display: this.state.editable ? "none" : "flex" }}>
                        {/*Options*/}
                        <UncontrolledTooltip placement="bottom" target={"tt-delete" + this.props.category._id}
                                             delay={{ show: 500, hide: 0 }}>
                            Delete
                        </UncontrolledTooltip>
                        <div className="category-icon"
                             id={"tt-delete" + this.props.category._id}
                             onClick={() => this.handleDeleteCategory()}>
                            <i className="fa fa-trash" aria-hidden="true" />
                        </div>

                        <UncontrolledTooltip placement="bottom" target={"tt-edit" + this.props.category._id}
                                             delay={{ show: 500, hide: 0 }}>
                            Edit
                        </UncontrolledTooltip>
                        <div className="category-icon"
                             id={"tt-edit" + this.props.category._id}
                             onClick={() => this.toggleEditable()}>
                            <i className="fa fa-pencil" aria-hidden="true" />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

CategoryItem.propTypes = {
    category: PropTypes.shape( {
        category: PropTypes.shape( {
            colour: PropTypes.shape( {
                name: PropTypes.string.isRequired,
                colourCode: PropTypes.string.isRequired,
            } ),
            name: PropTypes.string.isRequired,
        } ),
        created_at: PropTypes.string.isRequired,
        _id: PropTypes.string.isRequired,
        animateOnEntry: PropTypes.bool,
    } ),
};

export default CategoryItem;
