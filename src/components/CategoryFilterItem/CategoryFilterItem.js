import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './CategoryFilterItem.css';

class CategoryFilterItem extends Component {

    constructor( props ) {
        super( props );

        this.state = {};
    }

    componentDidMount() {
    }

    render() {

        const { colour, name } = this.props;

        return (
            <div className="category-filter-item" onClick={this.props.onClick}>
                <div className="colour-radio">
                    <div style={{ borderColor: colour }} />
                    <div className={this.props.selected ? "selected" : ""}
                         style={{ background: colour }} />
                </div>

                <div className="text-wrapper">
                    <span>{name}</span>
                    <span>notes: {this.props.noteCount}</span>
                </div>
            </div>
        );
    }
}

CategoryFilterItem.propTypes = {
    category: PropTypes.object,
    colour: PropTypes.string,
    name: PropTypes.string,
    noteCount: PropTypes.number,
    onClick: PropTypes.func,
};

export default CategoryFilterItem;
