export const initialiseFilters = categories => ( {
    type: 'INITIALISE_FILTERS',
    payload: categories,
} );

export const syncFilters = categories => ( {
    type: 'SYNC_FILTERS',
    payload: categories,
} );

export const setFilter = filter => ( {
    type: 'SET_FILTER',
    payload: filter,
} );

export const toggleFilter = category_id => ( {
    type: 'TOGGLE_FILTER',
    payload: category_id,
} );

export const setAllFilters = value => ( {
    type: 'SET_ALL_FILTERS',
    payload: value,
} );