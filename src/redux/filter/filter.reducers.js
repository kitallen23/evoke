const initialState = {
    filters: {
        no_category: false,
    },
};

export default ( state = initialState, { type, payload } ) => {
    switch ( type ) {

        case 'INITIALISE_FILTERS':
            let t_filters = { ...state.filters };
            payload.forEach( ( category ) => {
                t_filters[ category._id ] = false;
            } );

            return {
                ...state,
                filters: t_filters,
            };
        case 'SYNC_FILTERS':
            t_filters = { ...state.filters };
            let u_filters = [];

            payload.forEach( ( category ) => {
                if ( !t_filters.hasOwnProperty( category._id ) )
                    u_filters[ category._id ] = false;
                else
                    u_filters[ category._id ] = t_filters[ category._id ];
            } );

            return {
                ...state,
                filters: t_filters,
            };

        case 'SET_FILTER':
            return {
                ...state,
            };
        case 'TOGGLE_FILTER':
            return {
                ...state,
                filters: {
                    ...state.filters,
                    [ payload ]: !state.filters[ payload ],
                }
            };

        case 'SET_ALL_FILTERS':
            t_filters = { ...state.filters };

            for ( const key of Object.keys( t_filters ) ) {
                t_filters[ key ] = payload;
            }
            return {
                ...state,
                filters: t_filters,
            };

        default:
            return state;
    }
};