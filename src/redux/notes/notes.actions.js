export const addAllNotes = notes => ( {
    type: 'ADD_ALL_NOTES',
    payload: notes
} );

export const clearAnimateOnEntry = id => ( {
    type: 'CLEAR_ANIMATE_ON_ENTRY',
    payload: id
} );

export const addNote = note => ( {
    type: 'ADD_NOTE',
    payload: note
} );

export const updateNote = note => ( {
    type: 'UPDATE_NOTE',
    payload: note
} );

export const removeNote = id => ( {
    type: 'REMOVE_NOTE',
    payload: id
} );

/**
 * Takes a single object of shape:
 * {
 *      _id: note ID,
 *      display: bool,
 * }
 */
export const setNoteVisibility = value => ( {
    type: 'SET_NOTE_VISIBILITY',
    payload: value
} );

/**
 * Takes an array of shape:
 * {
 *      _id: note ID,
 *      display: bool,
 * }
 */
export const setMultiNoteVisibility = object => ({
    type: 'SET_MULTI_NOTE_VISIBILITY',
    payload: object
});
