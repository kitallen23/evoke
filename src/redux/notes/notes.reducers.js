const initialState = {
    notes: [],
};

export default ( state = initialState, { type, payload } ) => {
    switch ( type ) {

        case 'ADD_ALL_NOTES':
            let t_notes = payload;

            // Set all animations as false
            t_notes.forEach( function ( note ) {
                note.animateOnEntry = false;
                note.display = true;
            } );

            // Sort the array
            t_notes.sort( function ( a, b ) {
                let a_dt = new Date( typeof a.updated_at === "undefined" ? a.created_at : a.updated_at );
                let b_dt = new Date( typeof b.updated_at === "undefined" ? b.created_at : b.updated_at );

                return b_dt - a_dt;
            } );

            return {
                ...state,
                notes: t_notes,
            };

        case 'ADD_NOTE':
            let t_note = payload;

            // Set animation to true
            t_note.animateOnEntry = true;
            t_note.display = true;

            // Add note to array
            t_notes = [
                ...state.notes,
                payload,
            ];

            // Re-sort the array
            t_notes.sort( function ( a, b ) {
                let a_dt = new Date( typeof a.updated_at === "undefined" ? a.created_at : a.updated_at );
                let b_dt = new Date( typeof b.updated_at === "undefined" ? b.created_at : b.updated_at );

                return b_dt - a_dt;
            } );

            return {
                ...state,
                notes: t_notes,
            };

        case 'UPDATE_NOTE':
            return {
                ...state,
                notes: state.notes.map(
                    ( note ) => note._id === payload._id ? {
                        ...note,
                        title: payload.title,
                        note: payload.note,
                        type: payload.type,
                        category: payload.category,
                        ...(payload.updated_at ? {updated_at: payload.updated_at} : {}),
                    } : note
                )
            };

        case 'REMOVE_NOTE':
            // Create a copy
            t_notes = state.notes.slice();

            // Find index to remove
            let i = t_notes.findIndex( ( note ) => {
                return note._id === payload;
            } );

            // Remove element if found
            if ( i > -1 )
                t_notes.splice( i, 1 );

            return {
                ...state,
                notes: t_notes,
            };

        case 'CLEAR_ANIMATE_ON_ENTRY':
            return {
                ...state,
                notes: state.notes.map(
                    ( note ) => note._id === payload ? {
                        ...note,
                        animateOnEntry: false,
                    } : note
                )
            };
        case 'SET_NOTE_VISIBILITY':
            return {
                ...state,
                notes: state.notes.map(
                    ( note ) => note._id === payload._id ? {
                        ...note,
                        display: payload.display,
                    } : note
                )
            };
        case 'SET_MULTI_NOTE_VISIBILITY':
            return {
                ...state,
                // Map each note
                notes: state.notes.map(
                    // If note is contained in payload, set its display value, else return note
                    note => payload.some((el) => { return el._id === note._id; }) ? {
                        ...note,
                        display: payload.find((el) => el._id === note._id).display,
                    } : note
                )
            };
        default:
            return state;
    }
};