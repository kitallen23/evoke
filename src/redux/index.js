import { combineReducers } from 'redux';
import auth from './auth/auth.reducers';
import utility from './utility/utility.reducers';
import notes from './notes/notes.reducers';
import categories from './categories/categories.reducers';
import filter from './filter/filter.reducers';

import { routerReducer } from 'react-router-redux';

export default combineReducers({
    router: routerReducer,
    auth,
    utility,
    notes,
    categories,
    filter,
});