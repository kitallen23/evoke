const initialState = {
    user: null,
};

export default (state = initialState, {type, payload}) => {
    switch(type) {
        case 'SET_USER':
            return {
                ...state,
                user: payload,
            };
        case 'CLEAR_USER':
            return {
                ...state,
                user: null,
            };
        default:
            return state;
    }
};