const initialState = {
    errorMsg: {
        text: "",
        showErr: false,
    },
    showMenu: false,
    showNoteFilter: false,
    loading: {
        all: true,
        user: true,
        addingNote: false,
        addingCategory: false,
    },
    searchData: {
        text: "",
        performSearch: false,
        isSearching: false,
    }
};

export default (state = initialState, {type, payload}) => {
    switch(type) {

        case 'SET_ERROR_MSG':
            return {
                ...state,
                errorMsg: {
                    text: payload,
                    showErr: true,
                }
            };
        case 'CLEAR_ERROR_MSG':
            return {
                ...state,
                errorMsg: {
                    text: "",
                    showErr: false,
                }
            };

        case 'TOGGLE_MENU':
            return {
                ...state,
                showMenu: !state.showMenu,
            };
        case 'SET_MENU':
            return {
                ...state,
                showMenu: payload,
            };

        case 'TOGGLE_NOTE_FILTER':
            return {
                ...state,
                showNoteFilter: !state.showNoteFilter,
            };
        case 'SET_NOTE_FILTER':
            return {
                ...state,
                showNoteFilter: payload,
            };

        case 'SET_ALL_LOADING':
            return {
                ...state,
                loading: {
                    ...state.loading,
                    all: payload,
                },
            };
        case 'SET_USER_LOADING':
            return {
                ...state,
                loading: {
                    ...state.loading,
                    user: payload,
                },
            };
        case 'SET_ADDING_NOTE_LOADING':
            return {
                ...state,
                loading: {
                    ...state.loading,
                    addingNote: payload,
                },
            };
        case 'SET_ADDING_CATEGORY_LOADING':
            return {
                ...state,
                loading: {
                    ...state.loading,
                    addingCategory: payload,
                },
            };

        case 'SET_SEARCH_DATA':
            return {
                ...state,
                searchData: payload,
            };
        case 'SET_SEARCHING_STATUS':
            return {
                ...state,
                searchData: {
                    ...state.searchData,
                    isSearching: payload,
                }
            };
        default:
            return state;
    }
};