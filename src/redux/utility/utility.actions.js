export const setErrorMsg = text => ( {
    type: 'SET_ERROR_MSG',
    payload: text,
} );

export const clearErrorMsg = () => ( {
    type: 'CLEAR_ERROR_MSG',
} );

export const toggleMenu = () => ( {
    type: 'TOGGLE_MENU',
} );

export const setMenu = value => ( {
    type: 'SET_MENU',
    payload: value,
} );

export const toggleNoteFilter = () => ( {
    type: 'TOGGLE_NOTE_FILTER',
} );

export const setNoteFilter = value => ({
    type: 'SET_NOTE_FILTER',
    payload: value,
});

export const setAllLoading = value => ( {
    type: 'SET_ALL_LOADING',
    payload: value,
} );

export const setUserLoading = value => ( {
    type: 'SET_USER_LOADING',
    payload: value,
} );

export const setAddingNoteLoading = value => ( {
    type: 'SET_ADDING_NOTE_LOADING',
    payload: value,
} );

export const setAddingCategoryLoading = value => ( {
    type: 'SET_ADDING_CATEGORY_LOADING',
    payload: value,
} );

export const setSearchData = searchData => ( {
    type: 'SET_SEARCH_DATA',
    payload: searchData,
} );

export const setSearchingStatus = isSearching => ( {
    type: 'SET_SEARCHING_STATUS',
    payload: isSearching,
} );




