const initialSelectedColour = {
    name: "Red",
    colourCode: "#FF4136",
};
const blankCategory = {
    category: {
        name: "",
        colour: {
            name: "Add category",
            colourCode: "#eeeeee",
        }
    },
    _id: "no_category",
};

const initialState = {
    categories: [],
    categoriesLoaded: false,
    sortBy: "date created",

    selectedColour: initialSelectedColour,
    blankCategory: blankCategory,
};

export default ( state = initialState, { type, payload } ) => {
    switch ( type ) {

        case 'SET_SELECTED_CATEGORY_COLOUR':
            return {
                ...state,
                selectedColour: {
                    name: payload.name,
                    colourCode: payload.colourCode,
                }
            };

        case 'RESET_SELECTED_CATEGORY_COLOUR':
            return {
                ...state,
                selectedColour: initialSelectedColour
            };

        case 'ADD_ALL_CATEGORIES':
            let t_categories = payload;

            // Set all animations as false
            t_categories.forEach( function ( category ) {
                category.animateOnEntry = false;
            } );

            return {
                ...state,
                categories: t_categories,
            };

        case 'ADD_CATEGORY':
            return {
                ...state,
                categories: [
                    ...state.categories,
                    {
                        ...payload,
                        animateOnEntry: true,
                    },
                ]
            };

        case 'REMOVE_CATEGORY':
            // Create a copy
            t_categories = state.categories.slice();

            // Find index to remove
            let i = t_categories.findIndex( ( category ) => {
                return category._id === payload;
            } );

            // Remove element if found
            if ( i > -1 )
            {
                t_categories.splice( i, 1 );
            }

            return {
                ...state,
                categories: t_categories,
            };

        case 'UPDATE_CATEGORY':
            return {
                ...state,
                categories: state.categories.map(
                    ( category ) => category._id === payload._id ? {
                        ...category,
                        category: payload.category
                    } : category
                )
            };

        case 'SET_CATEGORIES_LOADED':
            return {
                ...state,
                categoriesLoaded: true,
            };

        case 'CLEAR_ANIMATE_ON_ENTRY':

            return {
                ...state,
                categories: state.categories.map(
                    ( category ) => category._id === payload ? {
                        ...category,
                        animateOnEntry: false,
                    } : category
                )
            };

        case 'CHANGE_SORT_BY':
            return {
                ...state,
                sortBy: payload,
            };

        default:
            return state;
    }
};