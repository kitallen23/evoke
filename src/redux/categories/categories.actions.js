export const setSelectedCategoryColour = colour => ( {
    type: 'SET_SELECTED_CATEGORY_COLOUR',
    payload: colour
} );

export const resetSelectedCategoryColour = () => ( {
    type: 'RESET_SELECTED_CATEGORY_COLOUR',
} );

export const addAllCategories = categories => ( {
    type: 'ADD_ALL_CATEGORIES',
    payload: categories
} );

export const addCategory = category => ( {
    type: 'ADD_CATEGORY',
    payload: category
} );

export const removeCategory = id => ( {
    type: 'REMOVE_CATEGORY',
    payload: id
} );

export const updateCategory = category => ( {
    type: 'UPDATE_CATEGORY',
    payload: category
} );

export const clearAnimateOnEntry = id => ({
    type: 'CLEAR_ANIMATE_ON_ENTRY',
    payload: id
});

export const changeSortBy = text => ({
    type: 'CHANGE_SORT_BY',
    payload: text
});