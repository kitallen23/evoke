
const quillColours = [
    // Grey /  bluegrey / red /      yellow /   green /    blue /     purple
    "#EEEEEE", "#CFD8DC", "#FFCDD2", "#FFECB3", "#DCEDC8", "#BBDEFB", "#D1C4E9",
    "#BDBDBD", "#90A4AE", "#E57373", "#FFD54F", "#AED581", "#64B5F6", "#9575CD",
    "#757575", "#607D8B", "#F44336", "#FFC107", "#8BC34A", "#2196F3", "#673AB7",
    "#424242", "#455A64", "#D32F2F", "#FFA000", "#689F38", "#1976D2", "#512DA8",
];

export const quillModules = {
    toolbar: [
        [ 'bold', 'italic', 'underline' ],
        [ { 'size': [ 'huge', 'large', false, 'small' ] }, { 'font': [] }, { 'color': quillColours } ],
        [ { 'indent': '-1' }, { 'indent': '+1' } ],
        [ { 'list': 'ordered' }, { 'list': 'bullet' } ],
        [ 'link', 'image', 'code-block' ]
    ],
    clipboard: {
        matchVisual: false
    },
    syntax: true,
};

export const quillFormats = [
    'size', 'font', 'color',
    'bold', 'italic', 'underline',
    'indent', 'list', 'bullet',
    'clean', 'link', 'code-block'
];