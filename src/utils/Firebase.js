import firebase from 'firebase';
import store from './../utils/configureStore';
import { setUser } from "../redux/auth/auth.actions";
import { setUserLoading } from "../redux/utility/utility.actions";
import { push } from 'react-router-redux';

let firebaseConfig = {
    apiKey: "AIzaSyBmjwRijWYg32OOGX_LMefApBhDTHDdDvQ",
    authDomain: "evoke-b5f6d.firebaseapp.com",
    databaseURL: "https://evoke-b5f6d.firebaseio.com",
    projectId: "evoke-b5f6d",
    storageBucket: "evoke-b5f6d.appspot.com",
    messagingSenderId: "881945106754"
};

class Firebase {

    googleProvider = new firebase.auth.GoogleAuthProvider();

    init() {
        firebase.initializeApp( firebaseConfig );

        // Listen for login or logout
        this.listenToAuthStateChanges();
    }

    listenToAuthStateChanges = () => {
        firebase.auth().onAuthStateChanged( this.handleAuthStateChange );
    };

    handleAuthStateChange = user => {
        if ( user ) {
            this.handleSuccessfulLogin( user );
        } else {
            this.handleSuccessfulLogout();
        }
        store.dispatch( setUserLoading( false ) );
    };

    handleSuccessfulLogin = user => {
        let t_user = {
            displayName: user.displayName,
            email: user.email,
            photoURL: user.photoURL,
            uid: user.uid,
            refreshToken: user.refreshToken,
        };
        store.dispatch( setUser( t_user ) );
    };

    /* Destroy all traces of an authenticated Firebase session. */
    handleSuccessfulLogout = () => {
        store.dispatch( setUser( null ) );
        store.dispatch( setUserLoading( true ) );

        // TODO: Clear all other data, such as notes, categories etc. in redux store
    };

    loginGoogle = () => {
        /* Redirect user to Firebase login page. */
        firebase.auth().signInWithPopup( this.googleProvider )
            .then( ( result ) => {
                console.log( result );
                store.dispatch( push( "/" ) );
            } )
            .catch( ( e ) => {
                // TODO: Update welcome screen to use Redux and set this error message here, so we can show the user
                // TODO: that login failed

                // store.dispatch( showMessage( e.message ));
            } );
    };

    logout = () => {
        firebase.auth().signOut().catch( ( err ) => {
            console.log( err );
        } );
    };
}

export default ( new Firebase() );