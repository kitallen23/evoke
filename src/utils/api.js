import firebase from 'firebase';
import { apiurl } from './constants';

export class NoteAPI {
    static CreateTextNote( note, category ) {
        return firebase.auth().currentUser.getIdToken( true ).then( ( idToken ) => {
            return fetch( apiurl + '/notes/add', {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify( {
                    idToken: idToken,
                    note: note,
                    category: category,
                    type: "textNote"
                } )
            } )
                .then( results => {
                    return results.json();
                } )
                .then( data => {
                    return data;
                } )
        } ).catch( ( error ) => {
            console.log( error );
        } );
    }

    static GetAllNotes() {
        return firebase.auth().currentUser.getIdToken( true ).then( ( idToken ) => {
            return fetch( apiurl + '/notes', {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify( {
                    idToken: idToken,
                } )
            } )
                .then( results => {
                    return results.json();
                } )
                .then( data => {
                    return data;
                } )
        } ).catch( ( error ) => {
            console.log( error );
        } );
    }

    static DeleteNote( id ) {
        return firebase.auth().currentUser.getIdToken( true ).then( ( idToken ) => {
            return fetch( apiurl + '/notes/' + id, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "DELETE",
                body: JSON.stringify( {
                    idToken: idToken,
                } )
            } )
                .then( results => {
                    return results.json();
                } )
                .then( data => {
                    return data;
                } )
        } ).catch( ( error ) => {
            console.log( error );
        } );
    }

    static UpdateNote( id, note ) {
        return firebase.auth().currentUser.getIdToken( true ).then( ( idToken ) => {
            return fetch( apiurl + '/notes/' + id, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "PUT",
                body: JSON.stringify( {
                    idToken: idToken,
                    note: note,
                } )
            } )
                .then( results => {
                    return results.json();
                } )
                .then( data => {
                    return data;
                } )
        } ).catch( ( error ) => {
            console.log( error );
        } );
    }

}

export class CategoryAPI {

    static CreateCategory( category ) {
        return firebase.auth().currentUser.getIdToken( true ).then( ( idToken ) => {
            return fetch( apiurl + '/categories/add', {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify( {
                    idToken: idToken,
                    category: category,
                } )
            } )
                .then( results => {
                    return results.json();
                } )
                .then( data => {
                    return data;
                } )
        } ).catch( ( error ) => {
            console.log( error );
        } );
    }

    static GetAllCategories() {
        return firebase.auth().currentUser.getIdToken( true ).then( ( idToken ) => {
            return fetch( apiurl + '/categories', {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify( {
                    idToken: idToken,
                } )
            } )
                .then( results => {
                    return results.json();
                } )
                .then( data => {
                    return data;
                } )
        } ).catch( ( error ) => {
            console.log( error );
        } );
    }

    static UpdateCategory( id, category ) {
        return firebase.auth().currentUser.getIdToken( true ).then( ( idToken ) => {
            return fetch( apiurl + '/categories/' + id, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "PUT",
                body: JSON.stringify( {
                    idToken: idToken,
                    category: category,
                } )
            } )
                .then( results => {
                    return results.json();
                } )
                .then( data => {
                    return data;
                } )
        } ).catch( ( error ) => {
            console.log( error );
        } );
    }

    static DeleteCategory( id ) {
        return firebase.auth().currentUser.getIdToken( true ).then( ( idToken ) => {
            return fetch( apiurl + '/categories/' + id, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "DELETE",
                body: JSON.stringify( {
                    idToken: idToken,
                } )
            } )
                .then( results => {
                    return results.json();
                } )
                .then( data => {
                    return data;
                } )
        } ).catch( ( error ) => {
            console.log( error );
        } );
    }
}