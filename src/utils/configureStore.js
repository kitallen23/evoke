import { applyMiddleware, createStore } from 'redux';
import logger from 'redux-logger';
import reducers from '../redux';
import { routerMiddleware } from 'react-router-redux';

import history from './history';

// Create router history middleware
const routerHistory = routerMiddleware(history);
const store = createStore(reducers, applyMiddleware(logger, routerHistory));

export default store;