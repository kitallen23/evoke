export default function searchNotes( notes, categories, searchData ) {
    const t_notes = [];

    if ( searchData.text === "" )
        return notes;

    let searchString = searchData.text.trim();
    const exactSearchPhrases = extractExactSearchPhrases( searchData.text );

    // Remove exact search phrases from the search string
    exactSearchPhrases.forEach( string => {
        searchString = searchString.replace( string, "" );
    } );
    // Remove remaining quotation marks
    searchString = searchString.replace( /["]+/g, "" );

    // Find remaining search phrases
    const searchPhrases = [];
    let t_searchPhrases = searchString.split( " " );
    t_searchPhrases.forEach( string => {
        if ( string !== "" )
            searchPhrases.push( string );
    } );

    let searchModifier = null;
    // Find modifier
    if ( searchPhrases.length > 0 ) {
        if ( searchPhrases[ 0 ].charAt( 0 ) === "@" ) {

            if ( isSearchModifier( searchPhrases[ 0 ] ) ) {
                searchModifier = searchPhrases[ 0 ];
                searchPhrases.splice( 0, 1 );
            }
        }
    }

    // TODO: Turn this into a switch statement for note.type
    notes.forEach( ( note ) => {
        if ( note.type === "textNote" ) {
            if ( !textNoteMatchesSearch( note, categories, searchModifier, searchPhrases, exactSearchPhrases, searchData ) ) {
                t_notes.push( {
                    ...note,
                    display: false,
                } );
            }
            else {
                t_notes.push( note );
            }
        }
    } );

    return t_notes;
}

function textNoteMatchesSearch( note, categories, searchModifier, searchPhrases, exactSearchPhrases, searchData ) {
    /**
     * Search strategy:
     *
     * If exact search phrases are present, ensure at least one exact phrase
     * is contained in the note. If an exact match is found, return true
     * if one (or more) regular search phrases is contained in the note.
     *
     * If no exact search phrases are present, simply ensure at least one
     * regular search phrase is contained in the note.
     **/

    switch ( searchModifier ) {
        case '@list-note':
            return false;
        case '@priority-note':
            return false;

        case '@category': {
            let categoryString = searchData.text.replace( "@category ", "" ).toLowerCase();

            let noteCategory = categories.find( ( category ) => {
                return category._id === note.category && contains( category.category.name.toLowerCase(), categoryString );
            } );

            return typeof noteCategory !== "undefined";
        }

        case '@code': {
            let codeInstances = extractTextFromElement( note.note, "pre" );
            let codeText = codeInstances.join( " " );

            // Return false immediately if no code sections found
            if ( codeInstances.length === 0 )
                return false;

            // Check if an exact search phrase is found
            if ( exactSearchPhrases.length > 0 ) {
                let exactPhraseMatches = 0;
                exactSearchPhrases.forEach( exactSearchPhrase => {
                    if ( contains( codeText, exactSearchPhrase ) )
                        ++exactPhraseMatches;
                } );

                // If an exact match was found, ensure all non-exact phrases match
                if ( exactPhraseMatches === exactSearchPhrases.length ) {
                    let phraseMatches = 0;
                    searchPhrases.forEach( searchPhrase => {
                        if ( contains( codeText, searchPhrase ) )
                            ++phraseMatches;
                    } );

                    // All exact phrases and all phrases matched this note
                    if ( phraseMatches === searchPhrases.length )
                        return true;
                }
            }
            else {
                let phraseMatches = 0;
                searchPhrases.forEach( searchPhrase => {
                    if ( contains( codeText, searchPhrase ) )
                        ++phraseMatches;
                } );

                // All phrases matched this note
                if ( phraseMatches === searchPhrases.length )
                    return true;
            }

            // No matches found, return false
            return false;
        }

        case '@title': {
            const titleText = note.title;

            // Check if an exact search phrase is found
            if ( exactSearchPhrases.length > 0 ) {
                let exactPhraseMatches = 0;
                exactSearchPhrases.forEach( exactSearchPhrase => {
                    if ( contains( titleText, exactSearchPhrase ) )
                        ++exactPhraseMatches;
                } );

                // If an exact match was found, ensure all non-exact phrases match
                if ( exactPhraseMatches === exactSearchPhrases.length ) {
                    let phraseMatches = 0;
                    searchPhrases.forEach( searchPhrase => {
                        if ( contains( titleText, searchPhrase ) )
                            ++phraseMatches;
                    } );

                    // All exact phrases and all phrases matched this note
                    if ( phraseMatches === searchPhrases.length )
                        return true;
                }
            }
            else {
                let phraseMatches = 0;
                searchPhrases.forEach( searchPhrase => {
                    if ( contains( titleText, searchPhrase ) )
                        ++phraseMatches;
                } );

                // All phrases matched this note
                if ( phraseMatches === searchPhrases.length )
                    return true;
            }

            // No matches found, return false
            return false;
        }

        case '@body': {
            const bodyText = strip( note.note );

            // Check if an exact search phrase is found
            if ( exactSearchPhrases.length > 0 ) {
                let exactPhraseMatches = 0;
                exactSearchPhrases.forEach( exactSearchPhrase => {
                    if ( contains( bodyText, exactSearchPhrase ) )
                        ++exactPhraseMatches;
                } );

                // If an exact match was found, ensure all non-exact phrases match
                if ( exactPhraseMatches === exactSearchPhrases.length ) {
                    let phraseMatches = 0;
                    searchPhrases.forEach( searchPhrase => {
                        if ( contains( bodyText, searchPhrase ) )
                            ++phraseMatches;
                    } );

                    // All exact phrases and all phrases matched this note
                    if ( phraseMatches === searchPhrases.length )
                        return true;
                }
            }
            else {
                let phraseMatches = 0;
                searchPhrases.forEach( searchPhrase => {
                    if ( contains( bodyText, searchPhrase ) )
                        ++phraseMatches;
                } );

                // All phrases matched this note
                if ( phraseMatches === searchPhrases.length )
                    return true;
            }

            // No matches found, return false
            return false;
        }

        case '@text-note':
        default: {
            const titleText = note.title;
            const bodyText = strip( note.note );

            // Check if an exact search phrase is found
            if ( exactSearchPhrases.length > 0 ) {
                let exactPhraseMatches = 0;
                exactSearchPhrases.forEach( exactSearchPhrase => {
                    if ( contains( titleText, exactSearchPhrase ) ) {
                        ++exactPhraseMatches;
                        return;
                    }
                    if ( contains( bodyText, exactSearchPhrase ) )
                        ++exactPhraseMatches;
                } );

                // If an exact match was found, ensure all non-exact phrases match
                if ( exactPhraseMatches === exactSearchPhrases.length ) {
                    let phraseMatches = 0;
                    searchPhrases.forEach( searchPhrase => {
                        if ( contains( titleText, searchPhrase ) ) {
                            ++phraseMatches;
                            return;
                        }
                        if ( contains( bodyText, searchPhrase ) )
                            ++phraseMatches;
                    } );

                    // All exact phrases and all phrases matched this note
                    if ( phraseMatches === searchPhrases.length )
                        return true;
                }
            }
            else {
                let phraseMatches = 0;
                searchPhrases.forEach( searchPhrase => {
                    if ( contains( titleText, searchPhrase ) ) {
                        ++phraseMatches;
                        return;
                    }
                    if ( contains( bodyText, searchPhrase ) )
                        ++phraseMatches;
                } );

                // All phrases matched this note
                if ( phraseMatches === searchPhrases.length )
                    return true;
            }

            // No matches found, return false
            return false;
        }
    }
}

function contains( string, substring ) {
    return string.toLowerCase().includes( substring.toLowerCase() );
}

function isSearchModifier( str ) {
    switch ( str ) {
        case '@title':
            return true;
        case '@body':
            return true;
        case '@text-note':
            return true;
        case '@list-note':
            return true;
        case '@priority-note':
            return true;
        case '@code':
            return true;
        case '@category':
            return true;
        // case '@link': return true;
        // case '@monospace': return true;
        // case '@serif': return true;
        // case '@sans-serif': return true;
        default:
            return false;
    }
}

function extractTextFromElement( str, nodeType ) {

    let tmp = document.createElement( "DIV" );
    tmp.innerHTML = str;

    let textNodes = tmp.getElementsByTagName( nodeType );

    let textInstances = [];

    for ( let i = 0; i < textNodes.length; ++i ) {
        textInstances.push( textNodes[ i ].textContent );
    }

    return textInstances;
}

function extractExactSearchPhrases( str ) {
    let quotations = [];

    for ( let i = 0; i < str.length; ++i ) {
        if ( str[ i ] === "\"" ) {
            quotations.push( i );
        }
    }

    let exactSearchPhrases = [];
    if ( quotations.length >= 2 ) {
        for ( let i = 0; i < quotations.length; i += 2 ) {
            if ( i + 1 >= quotations.length )
                continue;
            exactSearchPhrases.push( str.substring( quotations[ i ] + 1, quotations[ i + 1 ] ) );
        }
    }

    return exactSearchPhrases;
}

function strip( html ) {
    let tmp = document.createElement( "DIV" );
    tmp.innerHTML = html;
    return tmp.textContent || tmp.innerText;
}