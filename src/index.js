import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import WebFont from 'webfontloader';

import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';

import './index.css';
import App from './main';
import store from './utils/configureStore';
import history from './utils/history';

WebFont.load({
    google: {
        families: [
            'Roboto',
            'Roboto Slab',
            'Roboto Mono',
        ]
    }
});

ReactDOM.render((
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <App />
        </ConnectedRouter>
    </Provider>
), document.getElementById('root'));
registerServiceWorker();
