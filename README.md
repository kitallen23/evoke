
## Setup

After cloning the repository, run:<br />
`> npm install`

Then start the application with:<br />
`> npm start`

## The project

This project is a learning experience in tying together various frameworks, patterns and technologies. The core of this project uses the MERN (Mongo, Express, React, Node.js) stack.<br />
Please note that this project has only just begun to be developed. Many features are still a work-in-progress or are yet to be implemented.

The project is an enhanced note taking and todo list single-page application. Notes are able to be formatted using marked-up text via an integrated text editor.

The view is built with React, and Redux is being used for state management.

The back-end runs through an API using Node.js + Express + Mongo, located [here](https://bitbucket.org/kitallen23/evoke-api).

The MongoDB instance is hosted on mLab.

And finally, authorisation uses Google login with Firebase. The API uses Firebase Admin SDK to check authorisation.